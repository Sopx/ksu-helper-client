module.exports = {
    apps: [
        {
            name: 'ksu-helper-client',
            script: 'npm',
            args: 'start',
            log_date_format: 'YYYY-MM-DD HH:mm:ss',
        },
    ],
};
