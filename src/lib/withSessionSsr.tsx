import { IUserSnapshotIn, UserRoleEnum, UserRoleEnumArray } from "@/models/users/user.model";
import { ApiClientBase } from "@/services/api_client";
import { APP_ENV } from "@/services/app_env";
import { AppTranslationNamespaces } from "@/services/i18n";
import { isAxiosError } from "axios";
import { IncomingMessage } from "http";
import { GetServerSidePropsContext } from "next";

import { serverSideTranslations } from "next-i18next/serverSideTranslations";
import { NextApiRequestCookies } from "next/dist/server/api-utils";

interface NextApiRequestCookiesWithTokens extends NextApiRequestCookies {
  token: string;
}

export interface GetServerSidePropsContextApi extends GetServerSidePropsContext {
  api: ApiClientBase;
  req: IncomingMessage & {
    cookies: NextApiRequestCookiesWithTokens;
  };
  isLogged: boolean;
  locale: string;
  props: any;
}

interface NextApiRequestWithApi {
  api: ApiClientBase;
}

declare module "next" {
  interface NextApiRequest extends NextApiRequestWithApi {}
}

interface CustomProps<TProps> {
  props: TProps;
  redirect?: { destination: string };
  [key: string]: unknown;
}

const LOGIN_ROUTE = "/auth/login";
const MAIN_ROUTE = "/main";

export function withSessionSsr<P extends Record<string, any>>(
  ns: AppTranslationNamespaces[],
  handler: (
    context: GetServerSidePropsContextApi
  ) => CustomProps<P> | Promise<CustomProps<P>>,
  roles: UserRoleEnum[] = UserRoleEnumArray
) {
  const handlerWithApi = async (context: GetServerSidePropsContextApi) => {
    const isLogged = !!context.req.cookies.token;
    const api = new ApiClientBase(APP_ENV.SERVER_API_PATH);
    Object.assign(context, { api });

    let result = { props: {} };

    if (isLogged) {
      api.setToken(context.req.cookies.token);

      try {
        const { data } = await api.getProfile();
        const user = data as IUserSnapshotIn;
        Object.assign(result.props, { user });

        if (!user.roles || !roles.includes(user.roles[0])) {
          Object.assign(result, {
            redirect: {
              destination: MAIN_ROUTE,
            },
          });
        } else if (context.resolvedUrl.startsWith("/auth")) {
          Object.assign(result, {
            redirect: {
              destination: MAIN_ROUTE,
            },
          });
        }
      } catch (error) {
        if (isAxiosError(error)) {
          if (error.status === 401) {
            Object.assign(result, {
              redirect: {
                destination: LOGIN_ROUTE,
              },
            });
          } else if (error.status === 403) {
            Object.assign(result, {
              redirect: {
                destination: MAIN_ROUTE,
              },
            });
          }
        }
      }
    } else if (
      !context.resolvedUrl.startsWith("/auth") &&
      !context.resolvedUrl.startsWith("/main")
    ) {
      Object.assign(result, {
        redirect: {
          destination: LOGIN_ROUTE,
        },
      });
    }
    const translations = await serverSideTranslations(
      context.locale ?? "ru",
      ["common"].concat(ns)
    );

    const res = await handler(context as GetServerSidePropsContextApi);
    return {
      ...res,
      ...result,
      props: { ...res.props, ...translations, ...result.props },
    };
  };
  return handlerWithApi;
}
