import { i18n } from "i18next";
import { Settings } from "luxon";
import { useTranslation } from "next-i18next";
import { useEffect, useMemo } from "react";

import * as yup from "yup";

export type AppTranslationNamespaces =
  | "form"
  | "login"
  | "common"
  | "home"
  | "register"
  | "profile"
  | "vacancies"
  | "responses"
  | "forgot"
  | "utm"
  | "tariffs"
  | "payments"
  | "support-tickets"
  | "statistics"
  | "managers"
  | "hhru"
  | "integrations"
  | "crm";
export const useAppTranslation = (ns: AppTranslationNamespaces = "common") =>
  useTranslation(ns) as {
    t: (key: string, params?: any) => string;
    i18n: i18n;
  };

export const useYup = () => {
  const { t } = useAppTranslation("form");

  return useMemo(() => {
    yup.setLocale({
      mixed: {
        required: t("errors_required") ?? "errors_required",
      },
      string: {
        min: ({ min, path }) => {
          return t("errors_min", { min, label: t(`${path}_label`) });
        },
        max: ({ max, path }) => {
          return t("errors_max", { max, label: t(`${path}_label`) });
        },
        length: ({ length, path }) =>
          t("errors_length", { length, label: t(`${path}_label`) }),
        email: ({ path }) => {
          return t("errors_email", { label: t(`${path}_label`) });
        },
        matches: (shit) => {
          return shit.path === "email" ? t("errors_email"): "Shit";
        },
      },
      array: {
        min: ({ min, path }) =>
          t("errors_array_min", { min, label: t(`${path}_label`) }),
        max: ({ max, path }) =>
          t("errors_array_max", { max, label: t(`${path}_label`) }),
      },
      number: {
        min: ({ min, path }) => {
          return t("errors_number_min", { min, label: t(`${path}_label`) });
        },
        max: ({ max, path }) => {
          return t("errors_number_max", { max, label: t(`${path}_label`) });
        },
      },
    });

    return { appYup: yup };
  }, [t]);
};

export const useInitLocale = () => {
  const { i18n } = useAppTranslation();

  useEffect(() => {
    Settings.defaultLocale = i18n.language;
    // Settings.defaultZone = "Asia/Almaty";
  }, [i18n.language]);
};
