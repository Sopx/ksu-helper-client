import axios, {
  AxiosError,
  AxiosInstance,
  AxiosResponse,
  InternalAxiosRequestConfig,
} from "axios";
import { APP_ENV } from "./app_env";
import { NextRouter } from "next/router";
import NotificationService from "./notification_service";
import { parseCookies } from "nookies";
import { IParsedHtml } from "@/helpers";

let isRefreshing = false;
let isErrorShown = false;

export class ApiClientBase {
  token: string | null = null;
  refreshToken?: string | null = null;

  router?: NextRouter;

  setRouter(value: NextRouter) {
    this.router = value;
  }

  setToken(val: any) {
    this._api.defaults.headers.common.Authorization = `Bearer ${val}`;
  }

  private _api: AxiosInstance;

  constructor(baseURL: string) {
    this._api = axios.create({ baseURL, withCredentials: true });
    const responseInterceptor = {
      after: (response: AxiosResponse) => {
        // console.log(response.config.url, ":", response);
        if (response.data?.message && !response.data.id) {
          NotificationService.snackbar?.enqueueSnackbar(
            response?.data?.message,
            {
              variant: "success",
            }
          );
        }
        return response;
      },
      error: async (error: AxiosError<any>) => {
        console.log(error.config?.url, error.response?.status);
        switch (error.response?.status) {
          case 413:
            NotificationService.snackbar?.enqueueSnackbar(
              "Превышен лимит на размер файлов",
              {
                variant: "error",
              }
            );
            break;
          case 400:
          case 401:
            console.log(error.response.data);
            if (error.response?.data?.message) {
              if (!isErrorShown) {
                isErrorShown = true;
                NotificationService.snackbar?.enqueueSnackbar(
                  error.response?.data?.message,
                  {
                    variant: "error",
                    onClose: () => {
                      isErrorShown = false;
                    },
                  }
                );
                if (this.router && !this.router?.asPath.startsWith("/auth")) {
                  return this.router?.replace("/auth/login");
                }
              }
            }
            break;

          case 403:
            await this.logout();
            if (this.router) {
              return this.router.replace("/auth/login");
            }
            return Promise.reject({
              redirect: { destination: "/auth/login" },
            });

          case 401:
            if (!isRefreshing) {
              try {
                isRefreshing = true;

                if (this.router) {
                  return this.router.replace("/auth/login");
                }
                return Promise.reject({
                  redirect: { destination: "/auth/login" },
                });
                // const { headers } = await this.post("/auth/refresh");

                // const originalRequest = error.config!;
                // originalRequest.headers = originalRequest.headers.set(
                //   "cookie",
                //   headers["set-cookie"]
                // );

                // return this._api(originalRequest);
              } catch (error) {
                if (this.router) {
                  return this.router.replace("/auth/login");
                }
                return Promise.reject({
                  redirect: { destination: "/auth/login" },
                });
              } finally {
                isRefreshing = false;
              }
            }
            break;
          default:
            if (
              (error.response?.data?.message &&
                typeof error.response?.data?.message === "string") ||
              typeof error.response?.data === "string"
            ) {
              NotificationService.snackbar?.enqueueSnackbar(
                error.response?.data?.message || error.response?.data,
                {
                  variant: "error",
                }
              );
            } else {
              NotificationService.snackbar?.enqueueSnackbar(
                error.response?.statusText || error.status,
                {
                  variant: "error",
                }
              );
            }
            break;
        }

        return Promise.reject(error);
      },
    };

    const requestInterceptor = {
      pre: (config: InternalAxiosRequestConfig) => {
        const cookies = parseCookies();
        if (cookies.token) {
          config.headers.Authorization = `Bearer ${cookies.token}`;
        }
        return config;
      },
      error: (error: AxiosError) => {
        console.error(error);
        return Promise.reject(error);
      },
    };

    this._api.interceptors.request.use(
      requestInterceptor.pre,
      requestInterceptor.error
    );

    this._api.interceptors.response.use(
      responseInterceptor.after,
      responseInterceptor.error
    );
    this._api.interceptors;
  }

  get get() {
    return this._api.get;
  }

  get post() {
    return this._api.post;
  }

  logout() {
    return this._api.post("/auth/logout");
  }

  login(data: loginData) {
    return this._api.post(`/auth/login`, { ...data });
  }

  register(data: loginData) {
    return this._api.post(`/auth/registration`, { ...data });
  }

  getProfile() {
    return this._api.get("/users/me");
  }

  getScheduleByUserId(userId: string) {
    return this._api.get(`/get_user_schedule?userId=${userId}`);
  }

  getBotLogs(query?: string) {
    return this._api.get(`/get_user_activity_logs${query ?? ""}`);
  }

  createGroup(body: any) {
    return this._api.post("/groups", body);
  }

  getGroupById(id: string) {
    return this._api.get(`/groups/${id}`);
  }

  removeGroup(id: string) {
    return this._api.delete(`/groups/${id}`);
  }

  parseFile(formData: FormData) {
    return this._api.post("/tests/read-word-file", formData);
  }

  saveTest({
    questions,
    name,
    privacy,
  }: {
    questions: IParsedHtml[];
    name: string;
    privacy: string;
  }) {
    return this._api.post("/tests", { questions, name, privacy });
  }

  getTests() {
    return this._api.get("/tests");
  }

  getTestById(id: string) {
    return this._api.get(`/tests/${id}`);
  }

  deleteTest(id: string) {
    return this._api.delete(`/tests/${id}`);
  }
}

export const apiClient = new ApiClientBase(APP_ENV.SERVER_API_PATH);
