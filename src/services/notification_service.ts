import { ProviderContext } from "notistack";

type TFn = (key: string, params?: any) => string;

abstract class NotificationService {
  static snackbar: ProviderContext;
  private static t: TFn;
  static setSnackbar(snackbar: ProviderContext, t: TFn) {
    this.snackbar = snackbar;
    this.t = t;
  }
}

export default NotificationService;
