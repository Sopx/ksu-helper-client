import {
  Avatar,
  Box,
  Button,
  Container,
  IconButton,
  Menu,
  MenuItem,
  Stack,
  Tooltip,
  Typography,
} from "@mui/material";
import React, { ReactNode, useCallback, useEffect, useMemo } from "react";
import AppNavBar from "./AppNavBar";
import Image from "next/image";
import Link from "next/link";
import { observer } from "mobx-react";
import { rootStore } from "@/models/rootStore.model";
import MyLink from "./MyLink";
import { useAppTranslation } from "@/services/i18n";
import {
  onlyForAuthValues,
  onlyForUnAuthValues,
  useAccountMenu,
} from "./header/useAccountMenu";
import { apiClient } from "@/services/api_client";
import { useRouter } from "next/router";
import { destroyCookie, setCookie } from "nookies";

export default observer(() => {
  const user = rootStore.user;

  const AppAccountMenu = useAccountMenu();

  const { t } = useAppTranslation();

  const [anchorElUser, setAnchorElUser] = React.useState<null | HTMLElement>(
    null
  );
  const handleOpenUserMenu = (event: React.MouseEvent<HTMLElement>) => {
    setAnchorElUser(event.currentTarget);
  };
  const handleCloseUserMenu = () => {
    setAnchorElUser(null);
  };

  const logout = useCallback(async () => {
    try {
      destroyCookie(null, "token");
      await rootStore.getProfile();
    } catch (error) {
      console.log(error);
    }
  }, []);

  const checkCondition = rootStore.isAuth
    ? onlyForAuthValues
    : onlyForUnAuthValues;

  return (
    <Stack
      sx={{
        height: "inherit",
      }}
      alignItems={"center"}
      direction={"row"}
    >
      <Box sx={{ flexGrow: 0 }}>
        {rootStore.isAuth ? (
          <Tooltip title={t("menu")}>
            <IconButton onClick={handleOpenUserMenu} sx={{ p: 0 }}>
              <Avatar
                alt={`${user?.email}`}
                src="/static/images/avatar/2.jpg"
              />
            </IconButton>
          </Tooltip>
        ) : (
          <Button
            LinkComponent={MyLink}
            href="/auth/login"
            variant="text"
          >
            {t("buttons_login")}
          </Button>
        )}

        <Menu
          sx={{ mt: "45px" }}
          id="menu-appbar"
          anchorEl={anchorElUser}
          anchorOrigin={{
            vertical: "top",
            horizontal: "right",
          }}
          keepMounted
          transformOrigin={{
            vertical: "top",
            horizontal: "right",
          }}
          open={Boolean(anchorElUser)}
          onClose={handleCloseUserMenu}
        >
          {AppAccountMenu.map(
            (menuItem, i) =>
              checkCondition.includes(menuItem.auth) && (
                <MyLink key={i} to={menuItem.to} target={menuItem.target}>
                  <MenuItem onClick={handleCloseUserMenu}>
                    <Typography textAlign="center">
                      {t(menuItem.text)}
                    </Typography>
                  </MenuItem>
                </MyLink>
              )
          )}

          {rootStore.isAuth && (
            <MenuItem onClick={logout}>{t("header_menu_logout")}</MenuItem>
          )}
        </Menu>
      </Box>
    </Stack>
  );
});
