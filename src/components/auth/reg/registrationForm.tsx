import AppInput from "@/components/AppInput";
import {
  Button,
  FormControl,
  Input,
  InputLabel,
  Stack,
  Typography,
} from "@mui/material";
import { Formik, Form } from "formik";
import { useCallback, useState } from "react";
import Image from "next/image";
import Link from "next/link";
import { useRouter } from "next/dist/client/router";
import { useAppTranslation, useYup } from "@/services/i18n";
import { apiClient } from "@/services/api_client";
import { LoadingButton } from "@mui/lab";
import { rootStore } from "@/models/rootStore.model";
import { setCookie } from "nookies";

export default function RegistrationForm() {
  const router = useRouter();
  const [submitting, setSubmitting] = useState(false);
  const onSubmit = async ({ email, password }: any) => {
    try {
      setSubmitting(true);
      const response: any = await apiClient.register({ email, password });
      setCookie(null, "token", response.data.token, {
        secure: true,
        maxAge: 30 * 24 * 60 * 60,
        path: "/",
      });
      await rootStore.getProfile();
      await router.replace("/");
    } catch (error) {
      console.log(error);
    } finally {
      setSubmitting(false);
    }
  };

  const { appYup } = useYup();
  const { t } = useAppTranslation();

  const validationSchema = appYup.object({
    email: appYup
      .string()
      .matches(
        /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
      )
      .min(6)
      .required(),
    password: appYup
      .string()
      .min(4)
      .max(255)
      .oneOf(
        [appYup.ref("password_confirmation"), undefined],
        t("errors_passwords")
      )
      .required(),
    password_confirmation: appYup
      .string()
      .oneOf([appYup.ref("password"), undefined], t("errors_passwords"))
      .required(),
  });

  return (
    <Stack
      height={"inherit"}
      maxWidth={"70%"}
      alignItems={"center"}
      justifyContent={"center"}
    >
      <Stack mb={4} justifyContent={"center"} alignItems={"center"}>
        <Image
          src="/vercel.svg"
          width={40}
          height={40}
          alt={"Логотип"}
          style={{ marginBottom: "20px" }}
        />
        <Typography fontSize={24} fontWeight={500} color="black">
          {t("form:reg_pls")}
        </Typography>
        <Typography fontSize={14} fontWeight={300} color="gray">
          {t("form:for_get_access")}
        </Typography>
      </Stack>
      <Formik
        initialValues={{
          email: "",
          password: "",
          password_confirmation: "",
        }}
        validationSchema={validationSchema}
        onSubmit={onSubmit}
      >
        {() => {
          return (
            <Form>
              <AppInput name="email" label="email_label" type="text" />
              <AppInput
                name="password"
                label="password_label"
                placeholder="new_password_placeholder"
                type="password"
              />
              <AppInput
                name="password_confirmation"
                label="new_password_label"
                type="password"
              />
              <LoadingButton
                loading={submitting}
                fullWidth
                variant="contained"
                type="submit"
              >
                <Typography color={"white"} fontWeight={500}>
                  {t("form:sign_up")}
                </Typography>
              </LoadingButton>
            </Form>
          );
        }}
      </Formik>
    </Stack>
  );
}
