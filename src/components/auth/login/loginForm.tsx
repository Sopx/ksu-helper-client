import AppInput from "@/components/AppInput";
import {
  Button,
  FormControl,
  Input,
  InputLabel,
  Stack,
  Typography,
} from "@mui/material";
import { Formik, Form } from "formik";
import { useCallback, useEffect, useState } from "react";
import Image from "next/image";
import Link from "next/link";
import { useRouter } from "next/dist/client/router";
import { useAppTranslation, useYup } from "@/services/i18n";
import { apiClient } from "@/services/api_client";
import { LoadingButton } from "@mui/lab";
import { rootStore } from "@/models/rootStore.model";
import { setCookie } from "nookies";

export default function LoginForm() {
  const router = useRouter();
  const [submitting, setSubmitting] = useState(false);
  const onSubmit = async ({ email, password }: any) => {
    try {
      setSubmitting(true);
      const response: any = await apiClient.login({ email, password });
      setCookie(null, "token", response.data.token, {
        secure: true,
        maxAge: 30 * 24 * 60 * 60,
        path: "/",
      });
      await rootStore.getProfile();
      router.replace("/");
    } catch (error) {
      console.log(error);
    } finally {
      setSubmitting(false);
    }
  };

  const { appYup } = useYup();
  const { t } = useAppTranslation();

  const validationSchema = appYup.object({
    email: appYup.string().required(),
    password: appYup.string().required(),
  });

  return (
    <Stack height={"inherit"} alignItems={"center"} justifyContent={"center"}>
      <Stack mb={4} justifyContent={"center"} alignItems={"center"}>
        <Image
          src="/vercel.svg"
          width={40}
          height={40}
          alt={"Логотип"}
          style={{ marginBottom: "20px" }}
        />
        <Typography fontSize={24} fontWeight={500} color="black">
          {t("form:enter_pls")}
        </Typography>
        <Typography fontSize={14} fontWeight={300} color="gray">
          {t("form:for_get_access")}
        </Typography>
      </Stack>
      <Formik
        initialValues={{
          email: "",
          password: "",
        }}
        validationSchema={validationSchema}
        onSubmit={onSubmit}
      >
        {() => {
          return (
            <Form>
              <AppInput name="email" label="email_label" type="text" />
              <AppInput
                name="password"
                label="password_label"
                type="password"
              />
              {/* <Stack mb={1} alignItems={"end"} flexWrap={"nowrap"}>
                <Button onClick={() => router.push("/")}>
                  <Typography
                    sx={{
                      textTransform: "none",
                      fontWeight: 500,
                      fontSize: 14,
                      color: "#212281",
                    }}
                  >
                    {t("form:forgot_psw")}
                  </Typography>
                </Button>
              </Stack> */}
              <LoadingButton
                loading={submitting}
                fullWidth
                variant="contained"
                type="submit"
              >
                <Typography color={"white"} fontWeight={500}>
                  {t("form:sign_in")}
                </Typography>
              </LoadingButton>
              <Stack mt={1} alignItems={"center"} flexWrap={"nowrap"}>
                <Typography color="gray" fontSize={14}>
                  {t("form:have_no_account")}
                  <Link href="/auth/reg" style={{ color: "#212281" }}>
                    {t("form:sign_up")}
                  </Link>
                </Typography>
              </Stack>
            </Form>
          );
        }}
      </Formik>
    </Stack>
  );
}
