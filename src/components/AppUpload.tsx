import {
  Box,
  ButtonBase,
  FormControl,
  IconButton,
  InputLabel,
  Stack,
  Typography,
} from "@mui/material";
import { useFormikContext } from "formik";
import { FileDrop } from "react-file-drop";
import React, {
  ChangeEvent,
  useCallback,
  useEffect,
  useRef,
  useState,
} from "react";
import axios from "axios";
import { useAppTranslation } from "@/services/i18n";
import { apiClient } from "@/services/api_client";
import NotificationService from "@/services/notification_service";
interface Props {
  name: string;
  label?: string;
  parse?: boolean;
  accept?: string;
}

export default function AppUpload({
  name,
  label,
  accept,
  parse = false,
}: Props) {
  const { setFieldValue, getFieldMeta, values } = useFormikContext<any>();
  const meta = getFieldMeta<string>(name);
  const value = meta.value;
  const error = meta.error;
  const touched = meta.touched;

  const fileInputRef = useRef<HTMLInputElement>(null);
  const source = useRef(axios.CancelToken.source());
  const [state, setState] = useState<{
    tempFile?: File | null;
    progress?: number;
  }>({
    tempFile: null,
    progress: 0,
  });

  const { t } = useAppTranslation("form");

  const parseFile = useCallback(
    async (file?: File | null) => {
      if (file) {
        try {
          const formData = new FormData();
          formData.append("file", file);
          source.current = axios.CancelToken.source();
          const { data } = await apiClient.post("/read-word-file", formData, {
            onUploadProgress: (e) => {
              setState({
                progress: Math.floor((e.progress ?? 1) * 100),
                tempFile: file,
              });
            },
            cancelToken: source.current.token,
          });
          setFieldValue("html", data);
          NotificationService.snackbar.enqueueSnackbar(
            t("common:messages_file_uploaded"),
            {
              variant: "success",
              anchorOrigin: { horizontal: "center", vertical: "top" },
            }
          );
        } catch (error) {
          console.log(error);
        } finally {
          if (fileInputRef.current) {
            fileInputRef.current.value = "";
          }
        }
      }
    },
    [name, setFieldValue, t]
  );

  const uploadFile = useCallback(
    async (file?: File | null) => {
      if (file) {
        try {
          const formData = new FormData();
          formData.append("files", file);
          source.current = axios.CancelToken.source();
          const { data } = await apiClient.post("/files", formData, {
            onUploadProgress: (e) => {
              setState({
                progress: Math.floor((e.progress ?? 1) * 100),
                tempFile: file,
              });
            },
            cancelToken: source.current.token,
          });
          setFieldValue(name, data["filename"]);
          NotificationService.snackbar.enqueueSnackbar(
            t("common:messages_file_uploaded"),
            {
              variant: "success",
              anchorOrigin: { horizontal: "center", vertical: "top" },
            }
          );
        } catch (error) {
          console.log(error);
        } finally {
          if (fileInputRef.current) {
            fileInputRef.current.value = "";
          }
        }
      }
    },
    [name, setFieldValue, t]
  );

  useEffect(() => {
    const handlePasteAnywhere = async (event: ClipboardEvent) => {
      const clipboardItems = event.clipboardData?.items;
      const items: any[] = [].slice
        .call(clipboardItems)
        .filter(function (item: any) {
          return /^image\//.test(item.type);
        });
      if (items.length === 0) {
        return;
      }

      const item = items[0];
      const blob = item.getAsFile();
      parse ? parseFile(blob) : uploadFile(blob);
    };

    window.addEventListener("paste", handlePasteAnywhere);

    return () => {
      window.removeEventListener("paste", handlePasteAnywhere);
    };
  }, [parse, parseFile, uploadFile]);

  const handleFilesChange = (e: ChangeEvent<HTMLInputElement>) => {
    parse ? parseFile(e.target.files?.[0]) : uploadFile(e.target.files?.[0]);
  };

  const onTargetClick = useCallback(() => {
    fileInputRef.current?.click();
  }, []);

  const [isActive, setIsActive] = useState(false);

  const onDrop = useCallback(
    (files: FileList | null, event: React.DragEvent<HTMLDivElement>) => {
      setState({ progress: 0, tempFile: null });
      setIsActive(false);
      parse ? parseFile(files?.[0]) : uploadFile(files?.[0]);
    },
    [parse, parseFile, uploadFile]
  );
  const handleRemoveFile = useCallback(() => {
    source.current.cancel();
    setState({ progress: 0, tempFile: null });
    setFieldValue(name, null);
  }, [name, setFieldValue]);

  const onDragEnter = useCallback(() => {
    setIsActive(true);
  }, []);
  const onDragLeave = useCallback(() => {
    setIsActive(false);
  }, []);

  const _label = label && (t(label) ?? label);

  return (
    <FormControl
      fullWidth
      error={touched && Boolean(error)}
      variant="outlined"
      margin="normal"
    >
      {label && (
        <InputLabel
          shrink
          htmlFor={name}
          sx={{
            position: "relative",
            whiteSpace: "normal",
            left: -16,
          }}
        >
          {_label}
        </InputLabel>
        // <Typography variant="h4" mb={1}>
        //   {label}
        // </Typography>
      )}
      {!!state.progress || !!value ? (
        <Stack
          bgcolor={"background.default"}
          direction="row"
          alignItems="center"
          px={2}
          py={2}
          borderRadius={2}
          border={"1px solid"}
          borderColor="border.main"
        >
          <Stack sx={{ mx: 2, flex: 1 }}>
            <Typography variant="body2">
              {"Файл успешно импортирован" ?? state.tempFile?.name}
            </Typography>
            {!!state.tempFile?.size && (
              <Typography variant="caption">
                {/* {toFileSizeReadableFormat(state.tempFile?.size!)} */}
              </Typography>
            )}
            {state.progress != 100 &&
              !value &&
              //   <LinearProgressWithLabel value={state.progress ?? 0} />
              state.progress}
          </Stack>
          <Box sx={{ textAlign: "right" }}>
            <IconButton onClick={handleRemoveFile} sx={{ p: 0 }}>
              Мусорка
              {/* <TrashcanSvg /> */}
            </IconButton>
          </Box>
        </Stack>
      ) : (
        <FileDrop
          onTargetClick={onTargetClick}
          onFrameDragLeave={onDragLeave}
          onDragOver={onDragEnter}
          onDragLeave={onDragLeave}
          onDrop={onDrop}
        >
          <ButtonBase
            onMouseEnter={onDragEnter}
            onMouseLeave={onDragLeave}
            sx={{
              transition: "all 0.2s",
              backgroundColor: isActive ? "#EAFBF3" : "background.default",
              borderWidth: "2px",
              borderStyle: "dashed",
              borderColor:
                values[name] || isActive ? "#31D887" : "border.light",
              borderRadius: "10px",
              width: "100%",

              display: "flex",
              flexDirection: "column",
              alignItems: "center",
              justifyContent: "center",
              mt: 1,
              p: 2,
              color: isActive ? "text.green" : "text.secondary",

              "&:hover": {
                backgroundColor: "#EAFBF3",
                borderColor: "#31D887",
                color: "text.green",
              },
            }}
          >
            Картинка загрузки файла
            {/* <UploadIconSvg hover={isActive} /> */}
            <Box mt={1.5}>
              <Typography
                color="text.green"
                component="span"
                variant="subtitle2"
              >
                {`${t("common:drag_and_drop_file_select")} `}
              </Typography>
              <Typography component="span" variant="body2" color="inherit">
                {`${t("common:or")} ${t("common:drag_and_drop_file")}`}
              </Typography>
            </Box>
            {/* <Typography variant="caption" color="inherit">
                PNG, JPG (max. 800x400px)
              </Typography> */}
            <input
              hidden
              ref={fileInputRef}
              onChange={handleFilesChange}
              name={name}
              accept={accept ?? "*/*"}
              type="file"
            />
          </ButtonBase>
        </FileDrop>
      )}
    </FormControl>
  );
}
