import {IconButton, Menu, MenuItem, Typography} from "@mui/material";
import {observer} from "mobx-react";
import MoreHorizIcon from "@mui/icons-material/MoreHoriz";
import {useRef, useState} from "react";

interface Props {}

export default observer(({}: Props) => {
  const [anchorEl, setAnchorEl] = useState<null | HTMLElement>(null);

  const handleOpen = (event: React.MouseEvent<HTMLElement>) => {
    setAnchorEl(event.currentTarget);
  };
  const handleClose = () => {
    setAnchorEl(null);
  };

  return (
    <>
      <IconButton sx={{p: 0}} onClick={handleOpen}>
        <MoreHorizIcon fontSize="small" />
      </IconButton>

      <Menu
        open={!!anchorEl}
        onClose={handleClose}
        anchorEl={anchorEl}
        sx={{
          top: 10,
        }}
      >
        <MenuItem onClick={handleClose}>
          <Typography variant="body2">Пометить все как прочитанное</Typography>
        </MenuItem>
        <MenuItem onClick={handleClose}>
          <Typography variant="body2">Удалить все</Typography>
        </MenuItem>
      </Menu>
    </>
  );
});
