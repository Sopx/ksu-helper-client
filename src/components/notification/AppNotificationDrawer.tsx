import theme from "@/theme/theme";
import {
  Box,
  Container,
  Drawer,
  IconButton,
  Stack,
  Typography,
} from "@mui/material";
import {observer} from "mobx-react";
import MoreHorizIcon from "@mui/icons-material/MoreHoriz";
import NotificationMoreButton from "./NotificationMoreButton";

interface Props {
  open: boolean;
  toggleDrawer: (val: boolean) => void;
}

export default observer(({open, toggleDrawer}: Props) => {
  return (
    <Drawer
      open={open}
      onClose={toggleDrawer}
      anchor="right"
      sx={{".MuiDrawer-paper": {p: 0, borderRadius: 0, minWidth: 350}}}
    >
      <Stack
        direction="row"
        alignItems="center"
        justifyContent="space-between"
        p={3}
        borderBottom={`1px solid ${theme.palette.border.main}`}
        gap={6}
      >
        <Typography variant="h2">Уведомления</Typography>
        <NotificationMoreButton />
      </Stack>
      <Stack></Stack>
    </Drawer>
  );
});
