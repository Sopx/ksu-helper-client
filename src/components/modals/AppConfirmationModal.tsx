import { modalStore } from "@/models/modalStore.model";
import theme from "@/theme/theme";
import {
  Box,
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogContentText,
  DialogTitle,
  Stack,
} from "@mui/material";

import { observer } from "mobx-react";

export default observer(() => {
  return (
    <Dialog
      maxWidth="sm"
      open={modalStore.confirmation.visible}
      PaperProps={{
        sx: {
          [theme.breakpoints.down("sm")]: {
            "&.MuiPaper-root": {
              width: "calc(100% - 32px)",
              maxWidth: "100%",
            },
          },
          "&.MuiPaper-root": {
            padding: 0,
          },
        },
      }}
      onClose={modalStore.confirmation.hide}
      aria-labelledby="confirm-alert-dialog-title"
      aria-describedby="confirm-alert-dialog-description"
    >
      <DialogTitle id="confirm-alert-dialog-title">
        <Stack
          direction="row"
          justifyContent="center"
          alignItems={"center"}
          gap={2}
        >
          <Box>{modalStore.confirmation.title}</Box>
        </Stack>
      </DialogTitle>
      {Boolean(modalStore.confirmation.subtitle) && (
        <DialogContent>
          <DialogContentText whiteSpace="pre-line">
            {modalStore.confirmation.subtitle}
          </DialogContentText>
        </DialogContent>
      )}

      <DialogActions>
        <Button
          onClick={modalStore.confirmation.hide}
          variant="outlined"
          sx={{ flex: 1 }}
        >
          Отмена
        </Button>
        <Button
          variant="contained"
          onClick={modalStore.confirmation.onOk}
          autoFocus
          sx={{ flex: 1 }}
        >
          {modalStore.confirmation.okText}
        </Button>
      </DialogActions>
    </Dialog>
  );
});
