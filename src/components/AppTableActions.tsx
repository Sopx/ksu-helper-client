import { observer } from "mobx-react";

import MoreVertIcon from "@mui/icons-material/MoreVert";
import {
  Icon,
  IconButton,
  Menu,
  MenuItem,
  Stack,
  Typography,
} from "@mui/material";
import React, { useState } from "react";
import { useAppTranslation } from "@/services/i18n";

interface AppTableAction {
  label: string;
  onClick: () => void;
  icon: React.ReactElement;
}

export default observer(({ items }: { items: any[] }) => {
  const [anchorElUser, setAnchorElUser] = React.useState<null | HTMLElement>(
    null
  );
  const handleOpenUserMenu = (event: React.MouseEvent<HTMLElement>) => {
    setAnchorElUser(event.currentTarget);
  };
  const handleCloseUserMenu = () => {
    setAnchorElUser(null);
  };

  const { t } = useAppTranslation();

  return (
    <>
      <IconButton onClick={handleOpenUserMenu}>
        <MoreVertIcon />
      </IconButton>
      <Menu
        sx={{ mt: "45px" }}
        id="menu-appbar"
        anchorEl={anchorElUser}
        anchorOrigin={{
          vertical: "top",
          horizontal: "right",
        }}
        keepMounted
        transformOrigin={{
          vertical: "top",
          horizontal: "right",
        }}
        open={Boolean(anchorElUser)}
        onClose={handleCloseUserMenu}
      >
        {items.map((item: AppTableAction, index) => {
          const onClick = () => {
            item.onClick();
            handleCloseUserMenu();
          };
          return (
            <MenuItem key={index} onClick={onClick}>
              <Stack
                direction={"row"}
                justifyContent={"space-between"}
                alignItems={"center"}
                gap={2}
                sx={{
                  "& svg": {
                    width: 24,
                    height: 24,
                  },
                }}
              >
                {item.icon}
                <Typography textAlign="center" fontWeight={400}>
                  {t(item.label)}
                </Typography>
              </Stack>
            </MenuItem>
          );
        })}
      </Menu>
    </>
  );
});
