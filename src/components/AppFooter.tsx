import { Box, Stack, Typography } from "@mui/material";

export default function AppFooter({}) {
  const offset = 50;
  return (
    <>
      <Stack
        direction={"row"}
        justifyContent={"end"}
        alignItems={"center"}
        sx={{
          borderTop: "1px solid #e0e0e0",
          bgcolor: "white",
          px: 4,
          bottom: 0,
          left: 0,
          position: "absolute",
          zIndex: 1000,
          height: offset,
          width: "100%",
        }}
      >
        <Typography variant="body2">Made by Vladik & Artem</Typography>
      </Stack>
      <Box sx={{ minHeight: offset, width: "100%" }}></Box>
    </>
  );
}
