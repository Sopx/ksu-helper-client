import { useAppTranslation } from "@/services/i18n";
import {
  FormControl,
  FormHelperText,
  Input,
  InputLabel,
  InputProps,
  TextareaAutosize,
} from "@mui/material";
import { useFormikContext } from "formik";
import React, { useCallback, useEffect } from "react";
import { IMaskInput } from "react-imask";
import { IMask } from "react-imask";

interface CustomProps {
  onChange: (event: { target: { name: string; value: string } }) => void;
  name: string;
  value?: any;
}

export const NumberMaskCustom = React.forwardRef<HTMLElement, CustomProps>(
  function NumberMaskCustom(props, ref) {
    const { onChange, value, ...other } = props;

    const _onChange = useCallback(() => {}, []);
    const numberMaskString = IMask.createMask({
      mask: Number,
      scale: 0, // Number of decimal places (you can adjust this as needed)
      thousandsSeparator: " ", // Space as the thousands separator
      padFractionalZeros: false, // Do not add trailing zeros to the decimal part
      signed: false, // Whether negative numbers are allowed or not
    });

    const handleOnAccept = (value: string) => {
      const newNum = value.split(" ").join("");
      onChange({ target: { name: props.name, value: newNum } });
    };

    return (
      <IMaskInput
        mask={numberMaskString}
        overwrite
        value={String(value)}
        {...other}
        // defaultValue={value}
        type={"numeric"}
        inputMode={"numeric"}
        onChange={_onChange}
        // inputRef={ref}
        onAccept={(value: any) => {
          handleOnAccept(value);
        }}
      />
    );
  }
);

interface Props extends InputProps {
  name: string;
  label?: string;
  placeholder?: string;
  note?: string;
  value?: string;
}

export default function AppInput({
  name,
  label,
  placeholder,
  note,
  value: originalValue,
  inputProps,
  margin,
  onChange,
  ...props
}: Props) {
  const { handleChange, handleBlur, getFieldMeta, setFieldValue } =
    useFormikContext<any>();
  const meta = getFieldMeta(name);
  const value = meta.value ?? originalValue ?? "";
  const error = meta.error;
  const touched = meta.touched;

  const id = `${name}-helder-text`;

  const { t } = useAppTranslation("form");
  const _label = label && label && (t(label) ?? label);
  const _placeholder = placeholder ? t(placeholder) ?? placeholder : _label;

  const handleOnChange = useCallback(
    (e: any) => {
      handleChange(e);
      if (onChange) {
        onChange(e);
      }
    },
    [handleChange, onChange]
  );

  useEffect(() => {
    if (meta.value === "" && props.type === "number") {
      setFieldValue(name, null);
    }
  }, [meta.value, name, props.type, setFieldValue]);

  return (
    <FormControl
      fullWidth
      error={touched && Boolean(error)}
      required={props.required}
      size="small"
    >
      {Boolean(_label) && (
        <InputLabel shrink htmlFor={name}>
          {_label}
        </InputLabel>
      )}

      <Input
        disableUnderline
        onChange={handleOnChange}
        onBlur={handleBlur}
        aria-describedby={id}
        id={name}
        name={name}
        placeholder={_placeholder}
        inputComponent={
          props.type === "number" ? (NumberMaskCustom as any) : undefined
        }
        {...props}
        value={value}
        inputProps={{
          ...(inputProps ?? {}),
          style: props.multiline ? { resize: "vertical" } : {},
        }}
        size="small"
      />
      {/* @ts-ignore */}
      {!!error && (
        <FormHelperText error id={id}>
          {t(error) ?? error}
        </FormHelperText>
      )}
      {!!note && <FormHelperText id={id}>{t(note) ?? note}</FormHelperText>}
    </FormControl>
  );
}
