import { computed } from "mobx";
import { rootStore } from "@/models/rootStore.model";
import AccountCircleIcon from "@mui/icons-material/AccountCircle";
import EventNoteIcon from "@mui/icons-material/EventNote";
import LogoutIcon from "@mui/icons-material/Logout";
import { Avatar } from "@mui/material";

export enum isAuthorizedEnum {
  auth = "authOnly",
  none = "unAuthOnly",
  both = "both",
}

export const onlyForAuthValues = [isAuthorizedEnum.auth, isAuthorizedEnum.both];
export const onlyForUnAuthValues = [
  isAuthorizedEnum.none,
  isAuthorizedEnum.both,
];

export const useAccountMenu = () => {
  const appAccountMenus = computed(() => {
    const menus = [
      {
        text: "common:profile",
        to: "/profile",
        icon: <Avatar alt="Remmmyyy" src="/static/images/avatar/2.jpg" />,
        target: "_self",
        showNav: false,
        auth: isAuthorizedEnum.auth,
      },
      // {
      //   text: "Группа",
      //   to: rootStore.user?.group
      //     ? `/group/${rootStore.user?.group}`
      //     : "/group",
      //   target: "_self",
      //   showNav: true,
      //   auth: isAuthorizedEnum.auth,
      // },
      {
        text: "common:main",
        to: "/main",
        target: "_self",
        showNav: false,
        auth: isAuthorizedEnum.both,
      },
      {
        text: "common:schedule",
        to: "/schedule/bot",
        target: "_blank",
        icon: <EventNoteIcon fontSize="small" />,
        showNav: true,
        auth: isAuthorizedEnum.auth,
      },
      {
        text: "common:tests",
        to: "/tests",
        target: "_self",
        showNav: true,
        auth: isAuthorizedEnum.auth,
      }
      // {
      //   text: "common:schedule",
      //   to: "/schedule/bot",
      //   target: "_blank",
      //   icon: <EventNoteIcon fontSize="small" />,
      //   showNav: true,
      //   auth: isAuthorizedEnum.auth,
      // },
    ];
    // if (rootStore.user?.isCRMVisible) {
    //   menus.push({
    //     text: "common:crm",
    //     to: "/crm",
    //     icon: <RecentActorsOutlined fontSize="small" />,
    //     showNav: true,
    //   });
    // }
    return menus;
  }).get();

  return appAccountMenus;
};
