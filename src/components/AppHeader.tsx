import { Box, Stack } from "@mui/material";
import AppNavBar from "./AppNavBar";
import Image from "next/image";
import AppProfile from "./AppProfile";
import Link from "next/link";
import AppNotification from "./AppNotificationButton";
import { rootStore } from "@/models/rootStore.model";

export default function AppHeader({}) {
  const offset = 70;

  return (
    <>
      <Stack
        height={offset}
        direction={"row"}
        justifyContent={"space-between"}
        alignItems={"center"}
        sx={{
          bgcolor: "bg.header",
          px: 4,
          position: "fixed",
          top: 0,
          left: 0,
          zIndex: 1000,
          width: "100%",
        }}
      >
        <Stack direction={"row"} gap={4}>
          <Link href="/main">
            <Image
              src="/images/TolyanEdu.svg"
              alt="Логотип"
              width={64}
              height={64}
            />
          </Link>
          <AppNavBar />
        </Stack>
        <Stack direction={"row"} gap={4}>
          {rootStore.user && <AppNotification />}
          <AppProfile />
        </Stack>
      </Stack>
      <Box sx={{ minHeight: offset, width: "100%" }}></Box>
    </>
  );
}
