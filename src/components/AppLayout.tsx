import { Box, Container } from "@mui/material";
import { ReactNode } from "react";
import AppHeader from "./AppHeader";
import { observer } from "mobx-react";
import AppFooter from "./AppFooter";

interface Props {
  children: ReactNode;
  container?: boolean;
}

export default observer((props: Props) => {
  const { children, container = true } = props;
  return (
    <>
      <AppHeader />
      {container ? (
        <Box flex={1} py={2}>
          <Box sx={{ height: "100%", maxWidth: "1152px", margin: "0 auto" }}>
            {children}
          </Box>
        </Box>
      ) : (
        <Box flex={1}>{children}</Box>
      )}
      <AppFooter />
    </>
  );
});
