import { Stack, Typography } from "@mui/material";
import MyLink from "./MyLink";
import { useAppTranslation } from "@/services/i18n";
import {
  onlyForAuthValues,
  onlyForUnAuthValues,
  useAccountMenu,
} from "./header/useAccountMenu";
import { rootStore } from "@/models/rootStore.model";
import { observer } from "mobx-react";

export default observer(() => {
  const { t } = useAppTranslation();
  const AppAccoutMenu = useAccountMenu();

  const checkCondition = rootStore.isAuth
    ? onlyForAuthValues
    : onlyForUnAuthValues;

  return (
    <Stack direction={"row"} gap={2} height={"inherit"} alignItems={"center"}>
      {AppAccoutMenu.filter((eachMenu) => eachMenu.showNav).map(
        (menuItem, i) => {
          return (
            checkCondition.includes(menuItem.auth) && (
              <MyLink key={i} to={menuItem.to} target={menuItem.target}>
                <Typography variant="secondary">{t(menuItem.text)}</Typography>
              </MyLink>
            )
          );
        }
      )}
    </Stack>
  );
});
