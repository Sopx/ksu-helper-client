import {
  Box,
  Input,
  LinearProgress,
  Pagination,
  Paper,
  SxProps,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableFooter,
  TableHead,
  TableRow,
  Typography,
} from "@mui/material";
import { observer } from "mobx-react";
import { debounce } from "lodash";
import { useRouter } from "next/router";
import { useCallback, useEffect, useMemo, useState } from "react";
import { useAppTranslation } from "@/services/i18n";

export interface IAppTableColumn<T> {
  name?: string;
  label?: string | (() => React.ReactNode);
  width?: number;
  render?: (item: T) => React.ReactNode;
  sortable?: boolean;
  flex?: number;
}

interface Props<T> {
  list: any;
  columns: IAppTableColumn<T>[];
  onRowClick?: (item: T) => void;
  rowSxProps?: (item: T) => SxProps;
  showSearch?: boolean;
  chosenId?: string;
  showPhoneHint?: boolean;
}

export default observer(function <T>({
  list,
  columns,
  onRowClick,
  rowSxProps,
  showSearch = true,
  chosenId,
}: Props<T>) {
  const { t } = useAppTranslation();
  const router = useRouter();
  const query = router.query as any;
  const [searchValue, setSearchValue] = useState("");

  const handleOnPageChange = useCallback(
    (page: number) => {
      router.replace(
        {
          pathname: router.pathname,
          query: {
            ...list.queryJSON,
            ...router.query,
            page: page,
          },
        },
        undefined,
        { shallow: true }
      );
    },
    [list.queryJSON, router]
  );

  const onSearch = useMemo(() => {
    return debounce((val: string) => {
      router.replace(
        {
          pathname: router.pathname,
          query: {
            ...list.queryJSON,
            ...router.query,
            searchText: val,
            page: 1,
          },
        },
        undefined,
        { shallow: true }
      );
    }, 750);
  }, [list.queryJSON, router]);

  useEffect(() => {
    if (query.searchText === undefined) {
      setSearchValue("");
    }
  }, [query.searchText]);

  useEffect(() => {
    searchValue === ""
      ? router.replace(
          {
            pathname: router.pathname,
            query: {
              ...list.queryJSON,
              ...router.query,
              searchText: searchValue,
              page: router.query.page ?? 1,
            },
          },
          undefined,
          { shallow: true }
        )
      : onSearch(searchValue);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [searchValue]);

  useEffect(() => {
    list.getItems({ ...router.query });
  }, [router.query, list]);

  useEffect(() => {
    return () => {
      onSearch.cancel();
    };
  }, [onSearch]);

  const handleClick = useCallback(
    (item: T) => {
      onRowClick?.(item);
    },
    [onRowClick]
  );

  return (
    <Paper variant="outlined">
      {showSearch && (
        <Box p={2}>
          <Input
            onChange={(e: any) => setSearchValue(e.target.value)}
            fullWidth
            value={searchValue}
            placeholder={t("search_placeholder") ?? "search_placeholder"}
          />
        </Box>
      )}

      <TableContainer>
        <Table sx={{ borderCollapse: "separate" }}>
          <TableHead>
            <TableRow>
              {columns?.map((x, i) => (
                <TableCell width={x.width} key={i}>
                  {typeof x.label !== "string" ? x.label?.() : t(x.label) ?? ""}
                </TableCell>
              ))}
            </TableRow>
            {list.loading && (
              <TableRow>
                <TableCell colSpan={columns?.length}>
                  <LinearProgress />
                </TableCell>
              </TableRow>
            )}
          </TableHead>
          <TableBody>
            {list.items.length ? (
              list.items.map((x: any) => (
                <TableRow
                  sx={{
                    cursor: onRowClick ? "pointer" : "auto",
                    backgroundColor:
                      chosenId === x.id ? "#e2e2e2" : "transparent",
                    ...(rowSxProps ? rowSxProps(x) : {}),
                  }}
                  hover
                  key={x.id}
                  onClick={() => handleClick(x)}
                >
                  {columns?.map((c, i) => (
                    <TableCell width={c.width} key={i}>
                      {c.render ? (
                        c.render?.(x)
                      ) : (
                        <Typography
                          variant="subtitle2"
                          fontWeight={500}
                          color="table.text.default"
                        >
                          {x[`${c.name}`]}
                        </Typography>
                      )}
                    </TableCell>
                  ))}
                </TableRow>
              ))
            ) : (
              <TableRow>
                <TableCell colSpan={columns?.length}>
                  <Typography textAlign="center">Таблица пуста</Typography>
                </TableCell>
              </TableRow>
            )}
          </TableBody>
          <TableFooter>
            <TableRow>
              <TableCell colSpan={columns?.length}>
                <Pagination
                  sx={{ justifyContent: "end", display: "flex" }}
                  page={Number(list.query.page)}
                  count={list.totalPages}
                  disabled={list.loading || list.totalPages === 1}
                  onChange={(_, page) => handleOnPageChange(page)}
                />
              </TableCell>
            </TableRow>
          </TableFooter>
        </Table>
      </TableContainer>
    </Paper>
  );
});
