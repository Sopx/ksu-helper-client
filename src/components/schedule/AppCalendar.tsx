import React, { useState } from "react";
import { endOfDay, endOfWeek, startOfDay, startOfWeek } from "date-fns";
import { ToggleButton, ToggleButtonGroup, Typography } from "@mui/material";

const definedTimes = {
  today: startOfDay(new Date()).getDay(),
  startOfWeek: startOfWeek(new Date(), {
    weekStartsOn: 1,
  }),
  endOfWeek: endOfWeek(new Date(), {
    weekStartsOn: 1,
  }),
  startOfToday: startOfDay(new Date()),
  endOfToday: endOfDay(new Date()),
};
const definedDays = ["Пн", "Вт", "Ср", "Чт", "Пт", "Сб"];

interface Props {
  startDate?: Date | null;
  endDate?: string | null;
  onChange: (_: React.MouseEvent<HTMLElement>, value: number) => void;
  scheduleDay: number;
}

export default function AppCalendar({ onChange, scheduleDay }: Props) {
  return (
    <>
      <ToggleButtonGroup
        defaultValue={definedTimes.today}
        value={scheduleDay}
        onChange={onChange}
        exclusive
        fullWidth
      >
        {definedDays.map((day, i) => (
          <ToggleButton key={day} value={i}>
            {day}
          </ToggleButton>
        ))}
      </ToggleButtonGroup>
    </>
  );
}
