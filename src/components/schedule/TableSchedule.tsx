import { Grid, Stack, Typography } from "@mui/material";
import TableScheduleInfo from "./TableScheduleInfo";
import parse from "html-react-parser";
import themeTG, { tg } from "@/theme/themeTG";

interface ScheduleSubjectProps {
  time: string;
  subject: string;
}

interface ScheduleDateProps {
  day: string;
  subjects: ScheduleSubjectProps[];
}

interface ScheduleData {
  schedule: ScheduleDateProps[];
  updatedAt: number;
  group: {
    _id: string;
    name: string;
    id: number;
    language: string;
    href: string;
    age: number;
    studentCount: number;
    program: number;
    __v: number;
    createdAt: string;
    updatedAt: string;
  };
  user: {
    _id: string;
    userId: number;
    group: number;
    __v: number;
    createdAt: string;
    updatedAt: string;
    firstName: string;
    lastName: string;
    userType: string;
    username: string;
    isAdmin: boolean;
  };
}

interface SchedulePageProps {
  scheduleHash: ScheduleDateProps[];
  scheduleType: string;
  dayNum: number;
}

const definedColors =
  tg?.colorScheme === "light"
    ? ["#fff8dd", "#f0f9ff", "#fff0f0"]
    : ["#1c1c1f", "#202023", "#242427"];
export default function TableSchedule({
  scheduleHash,
  scheduleType,
  dayNum,
}: SchedulePageProps) {
  return (
    <>
      <Grid
        container
        rowGap={4}
        columnGap={2}
        justifyContent={"center"}
        pb={7.5}
        sx={{
          bgcolor:
            tg?.themeParams.secondary_bg_color ?? themeTG.palette.primary.main,
        }}
      >
        {scheduleHash?.map((date: any, i) => {
          return (scheduleType === "student" &&
            date.subjects.length === 0 &&
            dayNum === i) ||
            (scheduleType === "teacher" &&
              date.groups.length === 0 &&
              dayNum === i) ? (
            <Grid
              item
              key={i}
              sx={{
                position: "absolute",
                top: "40%",
              }}
            >
              <TableScheduleInfo index={i} text={`Выходной, чиллим😏)`} />
            </Grid>
          ) : (
            dayNum === i && (
              <Grid
                key={date.day}
                container
                item
                flex={1}
                mb={4}
                xs={12}
                md={3.5}
              >
                {scheduleType === "student"
                  ? date.subjects.map((subject: any, i: number) => {
                      return (
                        <Grid
                          container
                          item
                          key={subject.time}
                          xs={12}
                          justifyContent={"space-between"}
                          py={2}
                          sx={{
                            borderTop: "1px solid #a2a2a2",
                            borderBottom:
                              i === date.subjects.length - 1
                                ? "1px solid #a2a2a2"
                                : "none",
                            backgroundColor: subject.subject
                              ? definedColors[i % definedColors.length]
                              : "transparent",
                          }}
                          px={2}
                        >
                          <Grid item xs={3}>
                            <Typography fontSize={14} fontWeight={500}>
                              {subject.time}
                            </Typography>
                          </Grid>
                          <Grid item xs={9}>
                            <Typography
                              textAlign={"center"}
                              fontSize={16}
                              fontWeight={300}
                              lineHeight={1.2}
                            >
                              {parse(subject.subject)}
                            </Typography>
                          </Grid>
                        </Grid>
                      );
                    })
                  : date.groups.map((group: any, i: number) => {
                      return (
                        <Grid
                          container
                          item
                          key={i + group.time}
                          xs={12}
                          justifyContent={"center"}
                          py={2}
                          sx={{
                            borderTop: "1px solid #a2a2a2",
                            borderBottom:
                              i === date.groups.length - 1
                                ? "1px solid #a2a2a2"
                                : "none",
                            backgroundColor: group.group
                              ? definedColors[i % definedColors.length]
                              : "transparent",
                          }}
                          px={2}
                        >
                          <Grid item xs={3}>
                            <Typography fontSize={14} fontWeight={500}>
                              {group.time}
                            </Typography>
                          </Grid>
                          <Grid item xs={9}>
                            <Typography
                              textAlign={"center"}
                              fontSize={16}
                              lineHeight={1.2}
                              fontWeight={300}
                            >
                              {parse(group.group)}
                            </Typography>
                          </Grid>
                        </Grid>
                      );
                    })}
              </Grid>
            )
          );
        })}
      </Grid>
    </>
  );
}
