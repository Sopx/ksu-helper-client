import { Typography, Stack } from "@mui/material";
import themeTG from "@/theme/themeTG";

interface Props {
  index?: number;
  text: string;
}

export default function TableScheduleInfo({ index, text }: Props) {
  return (
    <Stack justifyContent="center" alignItems="center" key={index} flex={1}>
      <Typography
        fontSize={26}
        fontWeight={500}
        sx={{
          color: themeTG.palette.text.primary,
        }}
        textAlign={"center"}
      >
        {text}
      </Typography>
    </Stack>
  );
}
