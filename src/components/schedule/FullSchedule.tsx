import TableSchedule from "@/components/schedule/TableSchedule";
import { Typography, Tab, Stack } from "@mui/material";
import { useCallback, useState } from "react";
import AppCalendar from "@/components/schedule/AppCalendar";
import { startOfDay, startOfWeek, subDays } from "date-fns";
import { TabContext, TabList } from "@mui/lab";
import TableScheduleInfo from "./TableScheduleInfo";
import themeTG, { tg } from "@/theme/themeTG";

//@ts-ignore
export default function FullSchedule({ data }) {
  const [scheduleType, setScheduleType] = useState(
    data?.scheduleType ?? "student"
  );
  const [scheduleDay, setScheduleDay] = useState(
    startOfDay(new Date()).getDay() !== 6
      ? startOfDay(subDays(new Date(), 1)).getDay()
      : startOfWeek(subDays(new Date(), 1)).getDay()
  );

  const handleChangeTypeFilter = useCallback(
    (_: React.SyntheticEvent, newValue: string) => {
      setScheduleType(newValue);
    },
    []
  );

  const handleChangeDayFilter = useCallback(
    (_: React.MouseEvent<HTMLElement>, value: number) => {
      setScheduleDay((prev) => {
        return value === null ? prev : value;
      });
    },
    []
  );

  return (
    <Stack direction={"column"} height={"100%"} position="relative">
      {((scheduleType === "student" && !!data?.studentSchedule) ||
        (scheduleType === "teacher" && !!data?.teacherSchedule)) && (
        <Stack
          sx={{
            bgcolor:
              tg?.themeParams.button_color ?? themeTG.palette.primary.main,
          }}
          justifyContent={"center"}
          alignItems={"center"}
          py={1}
        >
          <Typography
            textAlign={"center"}
            fontWeight={700}
            fontSize={26}
            sx={{
              color:
                tg?.themeParams.button_text_color ?? themeTG.palette.text.white,
            }}
          >
            {scheduleType === "student"
              ? `${data.studentSchedule?.group.name}`
              : `${data.teacherSchedule?.teacher.name}`}
          </Typography>
        </Stack>
      )}

      <Stack
        sx={{
          flex: 1,
          bgcolor:
            tg?.themeParams.secondary_bg_color ?? themeTG.palette.primary.main,
          position: "relative",
        }}
      >
        {((scheduleType === "student" && !data?.studentSchedule) ||
          (scheduleType === "teacher" && !data?.teacherSchedule)) && (
          <TableScheduleInfo
            text={`❗️ У вас нет загруженного ранее расписания ${
              !data?.studentSchedule && scheduleType === "student"
                ? "группы!"
                : "преподавателя!"
            }`}
          />
        )}

        <TableSchedule
          dayNum={scheduleDay}
          scheduleType={scheduleType}
          scheduleHash={
            scheduleType === "student"
              ? data?.studentSchedule?.schedule
              : data?.teacherSchedule?.schedule
          }
        />
      </Stack>
      <Stack
        sx={{
          position: "fixed",
          top: "auto",
          bottom: 0,
          right: 0,
          left: 0,
          bgcolor: themeTG.palette.primary.main,
        }}
      >
        <TabContext value={scheduleType}>
          <TabList variant="fullWidth" onChange={handleChangeTypeFilter}>
            <Tab label={"Расписание группы"} value={"student"} />
            <Tab label={"Расписание препода"} value={"teacher"} />
          </TabList>
        </TabContext>
        <AppCalendar
          onChange={handleChangeDayFilter}
          scheduleDay={scheduleDay}
        />
      </Stack>
    </Stack>
  );
}
