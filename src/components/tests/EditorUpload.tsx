import {
  Box,
  ButtonBase,
  FormControl,
  IconButton,
  InputLabel,
  LinearProgress,
  Stack,
  Typography,
} from "@mui/material";
import { useFormikContext } from "formik";
import { FileDrop } from "react-file-drop";
import React, {
  ChangeEvent,
  useCallback,
  useEffect,
  useRef,
  useState,
} from "react";
import axios from "axios";
import { useAppTranslation } from "@/services/i18n";
import { apiClient } from "@/services/api_client";
import NotificationService from "@/services/notification_service";
import { UploadFile, Delete } from "@mui/icons-material";
interface Props {
  name: string;
  accept?: string;
}

export default function EditorUpload({ name, accept }: Props) {
  const { setFieldValue, getFieldMeta, values } = useFormikContext<any>();
  const meta = getFieldMeta<string>(name);
  const value = meta.value;
  const error = meta.error;
  const touched = meta.touched;

  const fileInputRef = useRef<HTMLInputElement>(null);
  const source = useRef(axios.CancelToken.source());
  const [state, setState] = useState<{
    tempFile?: File | null;
    progress?: number;
  }>({
    tempFile: null,
    progress: 0,
  });

  const [loading, setLoading] = useState(false);
  const { t } = useAppTranslation("form");

  const parseFile = useCallback(
    async (file?: File | null) => {
      if (file) {
        try {
          setLoading(true);
          const formData = new FormData();
          formData.append("file", file);
          const { data } = await apiClient.parseFile(formData);
          setFieldValue("html", data);
          NotificationService.snackbar.enqueueSnackbar(
            t("common:messages_file_uploaded"),
            {
              variant: "success",
              anchorOrigin: { horizontal: "right", vertical: "bottom" },
            }
          );
        } catch (error) {
          console.log(error);
        } finally {
          if (fileInputRef.current) {
            fileInputRef.current.value = "";
          }
          setLoading(false);
        }
      }
    },
    [setFieldValue, t]
  );

  const handleFilesChange = (e: ChangeEvent<HTMLInputElement>) => {
    parseFile(e.target.files?.[0]);
  };

  const onTargetClick = useCallback(() => {
    fileInputRef.current?.click();
  }, []);

  const [isActive, setIsActive] = useState(false);

  const onDrop = useCallback(
    (files: FileList | null, event: React.DragEvent<HTMLDivElement>) => {
      setState({ progress: 0, tempFile: null });
      setIsActive(false);
      parseFile(files?.[0]);
    },
    [parseFile]
  );
  const handleRemoveFile = useCallback(() => {
    source.current.cancel();
    setState({ progress: 0, tempFile: null });
    setFieldValue(name, null);
  }, [name, setFieldValue]);

  const onDragEnter = useCallback(() => {
    setIsActive(true);
  }, []);
  const onDragLeave = useCallback(() => {
    setIsActive(false);
  }, []);

  return (
    <FormControl
      fullWidth
      error={touched && Boolean(error)}
      variant="outlined"
      margin="normal"
    >
      {!!value ? (
        <Stack
          bgcolor={"background.default"}
          direction="row"
          alignItems="center"
          px={2}
          py={2}
          borderRadius={2}
          border={"1px solid"}
          borderColor="border.main"
        >
          <Stack sx={{ mx: 2, flex: 1 }}>
            <Typography variant="body2">
              {"Файл успешно импортирован" ?? state.tempFile?.name}
            </Typography>
            {/* {!!state.tempFile?.size && (
              <Typography variant="caption">
                {toFileSizeReadableFormat(state.tempFile?.size!)}
              </Typography>
            )} */}
            {/* {state.progress != 100 &&
              !value &&
              //   <LinearProgressWithLabel value={state.progress ?? 0} />
              state.progress} */}
          </Stack>
          <Box sx={{ textAlign: "right" }}>
            <IconButton onClick={handleRemoveFile} sx={{ p: 0 }}>
              <Delete />
            </IconButton>
          </Box>
        </Stack>
      ) : (
        <FileDrop
          onTargetClick={onTargetClick}
          onFrameDragLeave={onDragLeave}
          onDragOver={onDragEnter}
          onDragLeave={onDragLeave}
          onDrop={onDrop}
        >
          <ButtonBase
            onMouseEnter={onDragEnter}
            onMouseLeave={onDragLeave}
            sx={{
              transition: "all 0.2s",
              backgroundColor: isActive ? "#EAFBF3" : "background.default",
              borderWidth: "2px",
              borderStyle: "dashed",
              borderColor:
                values[name] || isActive ? "#31D887" : "border.light",
              borderRadius: "10px",
              width: "100%",

              display: "flex",
              flexDirection: "column",
              alignItems: "center",
              justifyContent: "center",
              mt: 1,
              p: 2,
              color: isActive ? "text.green" : "text.secondary",

              "&:hover": {
                backgroundColor: "#EAFBF3",
                borderColor: "#31D887",
                color: "text.green",
              },
            }}
          >
            <UploadFile />
            <Box mt={1.5}>
              <Typography
                color="text.green"
                component="span"
                variant="subtitle2"
              >
                {`${t("common:drag_and_drop_file_select")} `}
              </Typography>
              <Typography component="span" variant="body2" color="inherit">
                {`${t("common:or")} ${t("common:drag_and_drop_file")}`}
              </Typography>
              {loading && <LinearProgress />}
            </Box>
            <input
              hidden
              ref={fileInputRef}
              onChange={handleFilesChange}
              name={name}
              accept={accept ?? "*/*"}
              type="file"
            />
          </ButtonBase>
        </FileDrop>
      )}
    </FormControl>
  );
}
