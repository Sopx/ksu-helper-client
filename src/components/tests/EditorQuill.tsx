import { Box, Paper, Stack, Typography } from "@mui/material";
import { observer } from "mobx-react";
import { useQuill } from "react-quilljs";
import { useEffect, useRef, useState } from "react";
import "quill/dist/quill.snow.css";
import { useFormikContext } from "formik";
import { parseHtmlToJsonPLATONUS } from "@/helpers";

export default observer(({ html }: { html: string }) => {
  const { quillRef, quill } = useQuill();
  const { setFieldValue } = useFormikContext();

  const keyWords = useRef({
    keyWordForQuestion: "<question>",
    keyWordForAnswer: "<variant>",
  });

  const [cards, setCards] = useState(() =>
    parseHtmlToJsonPLATONUS(html, keyWords.current)
  );

  useEffect(() => {
    setCards(parseHtmlToJsonPLATONUS(html, keyWords.current));
  }, [html]);

  useEffect(() => {
    if (quill) {
      quill.clipboard.dangerouslyPasteHTML(html ?? "<></>");
      quill.on("text-change", () => {
        setCards(
          parseHtmlToJsonPLATONUS(quill.root.innerHTML, keyWords.current)
        );
      });

      return () => quill.off("text-change");
    }
  }, [html, keyWords, quill, quillRef, setFieldValue]);

  return (
    <Stack gap={2}>
      <Stack gap={2}>
        <Typography variant="title">HtmlEditor</Typography>
        <Box sx={{ maxHeight: 500, ".MuiBox-root": { maxHeight: 400 } }}>
          <Box ref={quillRef} />
        </Box>
      </Stack>
      {cards.map((card, index) => (
        <Stack key={index} direction={"row"} gap={2} alignItems={"center"}>
          <Typography variant={"h2"} color="text.secondary">
            {index + 1}
          </Typography>
          <Paper sx={{ flex: 1 }}>
            <Stack gap={3} direction="column" p={1}>
              <Stack direction={"row"} gap={1} alignItems={"start"}>
                <Typography sx={{ display: "inline" }} fontWeight={500}>
                  Вопрос:
                </Typography>
                <Typography
                  style={{ flex: 1 }}
                  dangerouslySetInnerHTML={{
                    __html: card.value.replaceAll("&lt;question&gt;", ""),
                  }}
                />
              </Stack>
              <Stack flex={1} gap={1}>
                {card.answers.map((answer, i: number) => (
                  <Stack
                    direction={"row"}
                    alignItems={"center"}
                    gap={1}
                    key={i}
                    sx={{ "& p": { display: "inline" } }}
                  >
                    <Typography
                      key={i}
                      sx={{ display: "inline" }}
                      fontWeight={answer.isCorrect ? 500 : 300}
                    >
                      Вариант {i + 1}:
                    </Typography>
                    <Typography
                      style={{ display: "inline" }}
                      dangerouslySetInnerHTML={{
                        __html: answer.value.replaceAll("&lt;variant&gt;", ""),
                      }}
                    />
                  </Stack>
                ))}
              </Stack>
            </Stack>
          </Paper>
        </Stack>
      ))}
    </Stack>
  );
});

// На лучший день ы
{
  /*  <Stack direction={"row"} gap={2}>
                  <AppInput
                    name="keyWordForQuestion"
                    label="Ключевое слово для вопроса"
                    placeholder="к примеру <question>"
                    onChange={handleInputChange}
                  />
                  <AppInput
                    name="keyWordForIncorrectAnswer"
                    label="Ключевое слово для неправильного варианта ответа"
                    placeholder="к примеру <variant>"
                    onChange={handleInputChange}
                  />
                  <AppInput
                    name="keyWordForCorrectAnswer"
                    label="Ключевое слово для правильного варианта ответа"
                    placeholder="к примеру <variantright>"
                    onChange={handleInputChange}
                  />
                </Stack> */
}
