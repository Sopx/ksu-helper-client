import { Box, Button, Drawer, Stack, Typography } from "@mui/material";
import { observer } from "mobx-react";
import { Form, Formik } from "formik";
import EditorUpload from "./EditorUpload";
import EditorQuill from "./EditorQuill";
import { parseHtmlToJsonPLATONUS } from "@/helpers";
import { apiClient } from "@/services/api_client";
import AppInput from "../AppInput";
import { useYup } from "@/services/i18n";
import NotificationService from "@/services/notification_service";
import { useState } from "react";

export default observer(
  ({ open, onClose }: { open: boolean; onClose: () => void }) => {
    const [loading, setLoading] = useState(false);
    const onSubmit = async (v: any) => {
      try {
        setLoading(true);
        const parsedHtml = parseHtmlToJsonPLATONUS(v.html, {
          keyWordForQuestion: "<question>",
          keyWordForAnswer: "<variant>",
        });

        await apiClient.saveTest({
          questions: parsedHtml,
          name: v.name,
          privacy: "private",
        });
        NotificationService.snackbar.enqueueSnackbar("Тест успешно создан", {
          variant: "success",
        });
      } catch (error) {
        console.error(error);
      } finally {
        onClose();
        setLoading(false);
      }
    };

    const { appYup } = useYup();

    const validationSchema = appYup.object({
      html: appYup.string().required(),
      name: appYup.string().required(),
    });

    return (
      <Drawer
        open={open}
        anchor="right"
        onClose={onClose}
        sx={{
          ".MuiPaper-root": {
            p: 0,
            maxWidth: 1200,
          },
        }}
      >
        <Formik
          validationSchema={validationSchema}
          initialValues={{
            html: "",
            name: "",
          }}
          onSubmit={onSubmit}
        >
          {({ values, submitForm }) => {
            return (
              <Form>
                <Stack
                  direction={"row"}
                  p={3}
                  borderBottom={"1px solid"}
                  borderColor="border.main"
                  justifyContent={"space-between"}
                  alignItems={"center"}
                  position={"sticky"}
                  top={0}
                  zIndex={1000}
                  sx={{ backgroundColor: "background.default" }}
                >
                  <Typography variant="h1">Создание теста</Typography>
                  <Stack direction="row" alignItems={"center"} gap={1}>
                    <Button
                      variant="contained"
                      onClick={submitForm}
                      disabled={loading}
                    >
                      Сохранить
                    </Button>
                    <Button
                      variant="contained"
                      onClick={onClose}
                      disabled={loading}
                    >
                      Отмена
                    </Button>
                  </Stack>
                </Stack>
                <Stack
                  direction={"row"}
                  p={3}
                  borderBottom={"1px solid"}
                  borderColor="border.main"
                  justifyContent={"space-between"}
                  alignItems={"center"}
                  gap={2}
                >
                  <Typography variant="h1" minWidth={"fit-content"}>
                    Импортировать файл
                  </Typography>
                  <EditorUpload name="html" accept=".doc,.docx" />
                </Stack>
                <Stack
                  direction={"row"}
                  p={3}
                  pb={1}
                  justifyContent={"space-between"}
                  alignItems={"center"}
                >
                  <AppInput name="name" label="Имя теста" required />
                </Stack>
                <Box p={3} borderBottom={"1px solid"} borderColor="border.main">
                  <EditorQuill html={values.html} />
                </Box>
              </Form>
            );
          }}
        </Formik>
      </Drawer>
    );
  }
);
