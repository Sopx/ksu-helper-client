import { FormControl, InputLabel, MenuItem, Select } from "@mui/material";
import React, { useCallback } from "react";
import { observer } from "mobx-react";
import { useAppTranslation } from "@/services/i18n";
import { useFormikContext } from "formik";

interface Props {
  name: string;
  label: string;
  items: any;
}

export default observer(({ name, label, items }: Props) => {
  const { t } = useAppTranslation();
  const { handleChange, getFieldMeta } = useFormikContext();
  const meta = getFieldMeta<string>(name);
  const value = meta.value ?? items[0];

  return (
    <FormControl fullWidth>
      {!!label && <InputLabel>{t(label)}</InputLabel>}
      <Select name={name} value={value} label={label} onChange={handleChange}>
        {items.map((x: any, i: number) => (
          <MenuItem key={i} value={x}>
            {t(x)}
          </MenuItem>
        ))}
      </Select>
    </FormControl>
  );
});
