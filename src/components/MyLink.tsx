// components/MyLink.tsx
import { LinkProps, Link as MuiLink } from "@mui/material";
import NextLink, { LinkProps as NextLinkProps } from "next/link";
import React from "react";
// Defining the CustomNextLink
export type CustomNextLinkProps = Omit<NextLinkProps, "href"> & {
  _href: NextLinkProps["href"];
};
export const CustomNextLink = React.forwardRef(
  ({ _href, ...props }: CustomNextLinkProps, ref: any) => {
    return <NextLink href={_href} {...props} ref={ref} />;
  }
);
CustomNextLink.displayName = "CustomNextLink";
// combine MUI LinkProps with NextLinkProps
type CombinedLinkProps = LinkProps<typeof NextLink>;
// remove both href properties
// and define a new href property using NextLinkProps
type MyLinkProps = Omit<CombinedLinkProps, "href"> & {
  to: NextLinkProps["href"];
  ref: any;
};
const MyLink = React.forwardRef(function Link(
  { to, ...props }: MyLinkProps,
  ref: any
) {
  return <MuiLink {...props} component={CustomNextLink} _href={to} ref={ref} />;
});

export default MyLink;
