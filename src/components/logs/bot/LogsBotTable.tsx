import { formatBeautifulDate } from "@/helpers";
import { logsInterface } from "@/models/logs/logs";
import { ExpandMore } from "@mui/icons-material";
import {
  Accordion,
  AccordionDetails,
  AccordionSummary,
  Box,
  Grid,
  Stack,
  Typography,
} from "@mui/material";
import React from "react";

interface LogsBotTableProps {
  logs: logsInterface;
}

export default function LogsBotTable({ logs }: LogsBotTableProps) {
  const recursiveShit = (fatha: any) => {
    const jsxElements = [];
    for (let item in fatha) {
      if (typeof fatha[item] !== typeof {}) {
        jsxElements.push(
          <Typography
            mb={1}
            key={`${item}: ${fatha[item]}`}
          >{`${item}: ${fatha[item]}`}</Typography>
        );
      } else {
        jsxElements.push(
          <Accordion
            key={`${item}: ${fatha[item]}`}
            sx={{
              bgcolor: "transparent",
              color: "inherit",
              px: 0,
              mx: 0,
              mb: 1,
              border: "1px solid black",
              ".MuiAccordionSummary-root.Mui-expanded": {
                minHeight: 0,
              },
              overflowX: "auto",
            }}
          >
            <AccordionSummary
              expandIcon={<ExpandMore sx={{ color: "#fff" }} />}
              sx={{
                display: "flex",
                gap: 2,
                p: 0,
                m: 0,
                minHeight: "0px",
                ".MuiAccordionSummary-content": {
                  margin: 0,
                },
              }}
            >
              <Typography>{item}</Typography>
            </AccordionSummary>
            <AccordionDetails>{recursiveShit(fatha[item])}</AccordionDetails>
          </Accordion>
        );
      }
    }

    return jsxElements;
  };

  return (
    <Box
      sx={{
        px: 1,
        py: 1,
        bgcolor: "#1a1d21",
        maxHeight: "100vh",
        overflowY: "auto",
      }}
    >
      <Stack direction="column" rowGap={0.5}>
        {logs &&
          logs?.documents?.map((log, i: number) => {
            return (
              <Grid container key={i} alignItems={"center"} flexWrap={"nowrap"}>
                <Grid item md={0.5} xs={1} textAlign={"center"}>
                  <Typography fontSize={16} fontWeight={500}>
                    {i < 9 ? `0${i + 1}.` : `${i + 1}.`}
                  </Typography>
                </Grid>
                {log.timestamp && (
                  <Grid item md={1.5} xs={2} textAlign={"center"}>
                    <Typography
                      sx={{
                        color: "#a2a2a2",
                        margin: "0px 40px 0px 20px",
                      }}
                      fontSize={14}
                    >
                      {formatBeautifulDate(log.timestamp)}
                    </Typography>
                  </Grid>
                )}
                <Grid item md={10} xs={9}>
                  <Accordion
                    sx={{
                      bgcolor: "transparent",
                      color: "inherit",
                      overflowX: "auto",
                      ".MuiAccordionSummary-root.Mui-expanded": {
                        minHeight: 24,
                      },
                      border: "1px solid black",
                    }}
                  >
                    <AccordionSummary
                      expandIcon={<ExpandMore sx={{ color: "#fff" }} />}
                      sx={{
                        display: "flex",
                        gap: 2,
                      }}
                    >
                      <Typography
                        sx={{
                          cursor: "pointer",
                          color:
                            log.level === "error"
                              ? "red"
                              : log.level === "warn"
                              ? "yellow"
                              : "inherit",
                        }}
                        fontSize={14}
                      >
                        {log.message}
                      </Typography>
                    </AccordionSummary>
                    <AccordionDetails>
                      <React.Fragment>{recursiveShit(log.meta)}</React.Fragment>
                    </AccordionDetails>
                  </Accordion>
                </Grid>
              </Grid>
            );
          })}
      </Stack>
    </Box>
  );
}
