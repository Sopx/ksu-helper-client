import { logsStatusLevelArray } from "@/models/logs/logs";
import { LogsBotStatusHandlerParamsInterface } from "@/models/logs/logs";
import { LoadingButton } from "@mui/lab";
import {
  Box,
  Checkbox,
  FormControl,
  Stack,
  TextField,
  Typography,
} from "@mui/material";
import { useCallback, useState, useEffect } from "react";

interface logsBotStatusHandlerInterface {
  finishFilters: (
    page: number,
    levels: typeof logsStatusLevelArray,
    limit: number
  ) => void;
  handleSetParams: (levels: typeof logsStatusLevelArray, limit: number) => void;
}

export default function LogsBotStatusHandler({
  finishFilters,
  handleSetParams,
}: logsBotStatusHandlerInterface) {
  const [loading, setLoading] = useState(false);
  const [params, setParams] = useState<LogsBotStatusHandlerParamsInterface>({
    levels: [],
    limit: 50,
  });

  const handleStatusChange = useCallback(
    (status: string) => {
      const uniqueArray: string[] = [];
      if (params.levels.length) {
        if (params.levels.includes(status)) {
          const levelIndex = params.levels.indexOf(status);
          uniqueArray.push(
            ...params.levels.filter((el, i) => i !== levelIndex)
          );
        } else {
          uniqueArray.push(status, ...params.levels);
        }
      } else {
        uniqueArray.push(status);
      }
      setParams((prev) => ({ ...prev, levels: uniqueArray }));
    },
    [params, setParams]
  );

  useEffect(() => {
    handleSetParams(params.levels, params.limit);
  }, [handleSetParams, params]);

  const _finishSelect = async () => {
    setLoading(true);
    await finishFilters(1, params.levels, params.limit);
    setLoading(false);
  };

  const onChange = useCallback((e: any) => {
    if (e.target.value > 0) {
      setParams((prev) => ({ ...prev, limit: e.target.value }));
      return;
    }
    setParams((prev) => ({ ...prev, limit: 50 }));
  }, []);

  return (
    <Box>
      <Stack direction={"row"} alignItems={"center"} gap={2}>
        {logsStatusLevelArray.map((status) => (
          <Stack key={status} direction={"row"} alignItems={"center"}>
            <Checkbox
              color="warning"
              onClick={() => handleStatusChange(status)}
            />
            <Typography>{status}</Typography>
          </Stack>
        ))}
        <FormControl
          sx={{
            ".MuiInputBase-root": {
              color: "#fff",
            },
            ".MuiFormLabel-root": {
              color: "#a2a2a2",
            },
            ".MuiOutlinedInput-notchedOutline": {
              borderColor: "#a2a2a2",
            },
          }}
        >
          <TextField
            placeholder={"Задайте лимит строк"}
            variant="outlined"
            label="Лимит строк"
            sx={{ mx: 4, color: "#fff" }}
            onChange={onChange}
          />
        </FormControl>

        <LoadingButton
          loading={loading}
          onClick={_finishSelect}
          variant="contained"
          color="success"
          sx={{
            ml: 4,
          }}
        >
          Применить фильтры
        </LoadingButton>
      </Stack>
    </Box>
  );
}
