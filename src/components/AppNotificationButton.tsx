import {Box, Container, IconButton, Stack} from "@mui/material";
import {ReactNode, useState} from "react";
import {observer} from "mobx-react";
import NotificationsActiveOutlinedIcon from "@mui/icons-material/NotificationsActiveOutlined";
import AppNotificationDrawer from "./notification/AppNotificationDrawer";

export default observer(({}) => {
  const [open, setOpen] = useState(false);
  const handleClick = () => {
    setOpen((prev) => !prev);
  };

  return (
    <>
      <IconButton sx={{p: 0}} onClick={handleClick}>
        <NotificationsActiveOutlinedIcon color="secondary" />
      </IconButton>
      <AppNotificationDrawer open={open} toggleDrawer={handleClick} />
    </>
  );
});
