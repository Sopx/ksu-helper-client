import { DateTime } from "luxon";
import * as cheerio from "cheerio";
import he from "he";
import { readFileSync } from "fs";

export const formatDate = (
  value: any,
  displayFormat = "dd.MM.yyyy HH:mm",
  valueFormat?: string
) => {
  if (!value) {
    return value;
  }
  const date = valueFormat
    ? DateTime.fromFormat(value, valueFormat)
    : DateTime.fromISO(value);

  return date.toFormat(displayFormat);
};

export const formatBeautifulDate = (value: any) => {
  if (!value) {
    return value;
  }
  const date = DateTime.fromISO(value);
  const humanReadable = date.toLocaleString(DateTime.DATETIME_MED_WITH_SECONDS);
  const formatted = humanReadable.split(",").join(" ");
  return formatted;
};

export function shuffleArray(array: any[]): void {
  for (let i: number = array.length - 1; i > 0; i--) {
    const j: number = Math.floor(Math.random() * (i + 1));
    [array[i], array[j]] = [array[j], array[i]];
  }
}

interface IParsedAnswerHtml {
  value: string;
  isCorrect: boolean;
}

export interface IParsedHtml {
  value: string;
  answers: IParsedAnswerHtml[];
}

export function parseHtmlToJsonPLATONUS(
  html: string,
  keyWords: {
    keyWordForQuestion: string;
    keyWordForAnswer: string;
  }
) {
  if (!html) {
    return [];
  }
  const $ = cheerio.load(html);

  const questions: IParsedHtml[] = [];

  const bodyTags = $("body > *").toArray();
  let questionSet = false;

  for (let i = 0; i < bodyTags.length; i++) {
    if ($(bodyTags[i]).text()?.includes(keyWords.keyWordForQuestion)) {
      let questionText = "<p>" + $(bodyTags[i]).html()?.trim() + "</p>";
      questionText.replace(/<[^>]*>?/gm, "");
      while (
        !$(bodyTags[i + 1])
          .text()
          ?.includes(keyWords.keyWordForAnswer) &&
        i < bodyTags.length
      ) {
        let text = $(bodyTags[i + 1])
          .html()
          ?.trim();
        questionText += text && text != "<br>" ? `<p>${text}</p>` : "";
        i++;
      }

      questions.push({
        value: questionText,
        answers: [],
      });
      questionSet = true;
    }
    if ($(bodyTags[i]).text()?.includes(keyWords.keyWordForAnswer)) {
      let answerText = "<p>" + $(bodyTags[i]).html()?.trim() + "</p>";
      while (
        !$(bodyTags[i + 1])
          .text()
          ?.includes(keyWords.keyWordForAnswer) &&
        i < bodyTags.length &&
        !$(bodyTags[i + 1])
          .text()
          ?.includes(keyWords.keyWordForQuestion)
      ) {
        let text = $(bodyTags[i + 1])
          .html()
          ?.trim();
        answerText += text && text != "<br>" ? `<p>${text}</p>` : "";
        i++;
      }
      questions[questions.length - 1].answers.push({
        value: answerText,
        isCorrect: questionSet,
      });
      questionSet = false;
    }
  }

  return questions;
}