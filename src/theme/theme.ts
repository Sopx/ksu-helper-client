import { createTheme } from "@mui/material/styles";

import { ruRU } from "@mui/material/locale";

declare module "@mui/material/Typography" {
  interface TypographyPropsVariantOverrides {
    label: true;
    title: true;
    pageTitle: true;
    hint: true;
    primary: true;
    secondary: true;
  }
}
declare module "@mui/material/styles" {
  interface Palette {
    border: Palette["primary"];
    button: Palette["primary"];
    input: PaletteOptions["input"];
    bg: PaletteOptions["bg"];
  }
  interface PaletteOptions {
    border: PaletteOptions["primary"];
    button: PaletteOptions["primary"];
    input: TypeInput;
    bg?: TypeBackgroundCustom;
  }

  interface TypeText {
    gray: string;
    white: string;
    black: string;
    placeholder: string;
  }

  interface PaletteColor {
    background: string;
  }

  interface SimplePaletteColorOptions {
    background?: string;
    text?: string;
  }

  interface TypeInput {
    background: {
      default: string;
      hover: string;
    };
    border?: {
      default: string;
      hover: string;
    };
    text: {
      default: string;
      hover: string;
      black: string;
    };
    placeholder: string;
  }
}

declare module "@mui/material/Paper" {
  interface PaperPropsVariantOverrides {
    simple: true;
    item: true;
    small: true;
    fullpage: true;
  }
}

const themePalette = createTheme({
  typography: {
    // fontFamily: fontFamily,

    h1: {
      fontSize: 24,
      lineHeight: "130%",
      fontWeight: 500,
    },
    h2: {
      fontSize: 20,
      lineHeight: 1,
      fontWeight: 500,
    },
    h3: {
      fontSize: 24,
      lineHeight: "120%",
      fontWeight: 600,
    },
    h6: {
      fontSize: 21,
      fontWeight: 500,
      lineHeight: 1.2,
    },
    subtitle1: {
      fontSize: 16,
      lineHeight: 1.2,
      fontWeight: 600,
    },
    fontSize: 16,
    body1: {
      fontWeight: "300",
      fontSize: 16,
    },
    body2: {
      fontWeight: 400,
      fontSize: 14,
      lineHeight: 1.2,
    },
    subtitle2: {
      fontSize: 14,
      lineHeight: "120%",
      fontWeight: "600",
    },
    caption: {
      fontSize: 12,
      lineHeight: 1.2,
      fontWeight: 400,
    },
  },
  palette: {
    button: {
      main: "inherit",
      text: "#000",
      light: "#E3E8EB",
      dark: "#000000",
      contrastText: "#fff",
    },
    background: {
      default: "#F7F7F8",
      paper: "#fbfbfb",
    },
    bg: {
      header: "#000",
      default: "#101010",
    },
    primary: {
      main: "#101418",
    },
    secondary: {
      main: "#ffffff",
      background: "#F7FEFA",
    },
    input: {
      background: {
        default: "#FFFFFF",
        hover: "#FFFFFF",
      },
      text: {
        default: "#090A0D",
        hover: "#090A0D",
        black: "#000000",
      },
      border: {
        default: "#EAECF0",
        hover: "#007bff",
      },
      placeholder: "#667085",
    },
    text: {
      primary: "#000",
      secondary: "#878787",
      gray: "#828282",
      white: "#fff",
      black: "#000",
    },
    border: {
      main: "#E2E2E2",
    },
  },
});
// Create a theme instance.
const theme = createTheme(
  {
    ...themePalette,

    breakpoints: {
      values: {
        xs: 0,
        sm: 600,
        md: 900,
        lg: 1200,
        xl: 1420,
      },
    },
    components: {
      MuiPaper: {
        styleOverrides: {
          root: {
            borderRadius: themePalette.shape.borderRadius * 2,
            borderWidth: 1,
            borderStyle: "solid",
            borderColor: themePalette.palette.border.main,
            boxShadow: "0px 5px 10px 5px rgba(0, 0, 0, 0.08)",
            padding: "16px",
            backgroundColor: "#f7f7f7",
          },
        },
      },
      MuiButton: {
        variants: [
          {
            props: {
              variant: "contained",
            },
            style: {},
          },
          {
            props: {
              variant: "outlined",
            },
            style: {
              color: themePalette.palette.text.primary,
            },
          },
        ],
        styleOverrides: {
          root: {
            color: themePalette.palette.text.white,
            textTransform: "none",
          },
        },
      },
      MuiTabs: {
        styleOverrides: {
          root: {
            "& .MuiTabs-indicator": {
              backgroundColor: themePalette.palette.text.primary,
            },
          },
        },
      },
      MuiCard: {
        styleOverrides: {
          root: {
            borderRadius: themePalette.shape.borderRadius * 3,
            borderWidth: 1,
            borderStyle: "solid",
            borderColor: themePalette.palette.border.main,
          },
        },
        variants: [
          {
            props: { variant: "outlined" },
            style: {},
          },
          {
            props: {
              variant: "simple",
            },
            style: {
              "& .MuiCardHeader-root": {
                padding: themePalette.shape.borderRadius * 6,
              },
              "& .MuiCardContent-root": {
                padding: themePalette.shape.borderRadius * 6,
              },
              "&.MuiPaper-root": {
                padding: 0,
              },
            },
          },
          {
            props: { variant: "fullpage" },
            style: {
              "& .MuiCardHeader-root": {
                padding: themePalette.shape.borderRadius * 6,
              },
              "& .MuiCardContent-root": {
                padding: themePalette.shape.borderRadius * 6,
                flex: 1,
                display: "flex",
                flexDirection: "column",
              },
              "&.MuiPaper-root": {
                padding: 0,
              },
              height: "100%",
              display: "flex",
              flexDirection: "column",
            },
          },
        ],
      },
      MuiCardContent: {
        styleOverrides: {
          root: {
            [themePalette.breakpoints.down("md")]: {
              padding: "12px",
            },
            [themePalette.breakpoints.up("md")]: {
              padding: "16px 16px",
            },
          },
        },
      },
      MuiCardActions: {
        styleOverrides: {
          root: {
            borderTop: `1px solid ${themePalette.palette.border.main}`,
            padding: "16px 24px",
          },
        },
      },
      MuiCardHeader: {
        defaultProps: {
          titleTypographyProps: {
            variant: "subtitle1",
            fontWeight: 500,
          },
        },
        styleOverrides: {
          root: {
            borderBottomWidth: 1,
            borderBottomStyle: "solid",
            borderBottomColor: themePalette.palette.border.main,
            [themePalette.breakpoints.down("sm")]: {
              padding: "12px",
            },
          },
        },
      },
      MuiTab: {
        styleOverrides: {
          root: {
            textTransform: "none",
            "&.Mui-selected": {
              color: themePalette.palette.text.primary,
            },
          },
        },
      },
      MuiToggleButton: {
        styleOverrides: {
          root: {
            textTransform: "none",
            border: "none",
            backgroundColor: themePalette.palette.background.paper,
            color: themePalette.palette.text.gray,
            "&.Mui-selected": {
              backgroundColor: "#fff",
              color: "#000",
              ":hover": {
                backgroundColor: themePalette.palette.primary.main,
              },
            },
            ":hover": {
              backgroundColor: themePalette.palette.primary.main,
              color: themePalette.palette.text.white,
            },
          },
        },
      },
      MuiFormControl: {
        styleOverrides: {
          root: {
            marginBottom: "16px",
            "& .MuiInputLabel-root": {
              fontSize: "16px",
            },
          },
        },
      },
      MuiMenu: {
        styleOverrides: {
          paper: {
            padding: "0px",
          },
          list: {
            padding: "0px",
          },
        },
      },
      MuiMenuItem: {
        styleOverrides: {
          root: {
            padding: "12px",
          },
        },
      },
      MuiInputBase: {
        styleOverrides: {
          root: {
            "&.MuiInput-root": {
              marginTop: 12,
              ".MuiInputAdornment-positionStart": {
                paddingLeft: "12px",
                marginRight: "0",
              },
              ".MuiInputAdornment-positionEnd": {
                paddingRight: "20px",
                marginLeft: "4px",
              },
            },
            "&.MuiOutlinedInput-root": {
              borderRadius: "8px",
            },
            fontSize: 14,
            fontWeight: 400,
            border: `1px solid ${themePalette.palette.border.main}`,
            backgroundColor: "#fff",
            borderRadius: "8px",
            color: themePalette.palette.input.text.black,
            "input::placeholder": {
              color: themePalette.palette.input.placeholder,
            },
            "&.MuiAutocomplete-inputRoot": {
              ".MuiAutocomplete-input": {
                padding: 0,
                borderRadius: "8px",
              },
              padding: "10px 14px",
              borderRadius: "8px",
              borderWidth: 0,
              ".MuiOutlinedInput-notchedOutline": {
                border: `1px solid ${themePalette.palette.border.main}`,
              },
            },
            "&.Mui-focused.MuiAutocomplete-inputRoot": {
              ".MuiOutlinedInput-notchedOutline": {
                borderWidth: "2px !important",
              },
            },
            "&.MuiInputBase-formControl .MuiSelect-nativeInput+.MuiSvgIcon-root":
              {
                marginRight: 16,
              },

            "&.MuiInput-input:focus": {
              borderRadius: "8px",
            },
            "&.Mui-focused": {
              ".MuiSelect-select": {
                backgroundColor: "#fff",
              },
              ".MuiOutlinedInput-notchedOutline": {
                borderWidth: "0 !important",
              },
              borderColor: themePalette.palette.input.border?.hover,
              boxShadow: `0px 0px 0px 3px ${themePalette.palette.input.border?.hover}14`,
            },

            "& .MuiInputBase-input": {
              padding: "10px 14px",
              borderRadius: "8px",
              ":focus": {
                borderRadius: "8px",
              },
            },
            ".MuiOutlinedInput-notchedOutline": {
              borderRadius: "8px",
              borderWidth: 0,
            },
            "&.Mui-disabled": {
              ".MuiOutlinedInput-notchedOutline": {
                borderRadius: "8px",
                // border: "initial",
              },
              backgroundColor: themePalette.palette.input.background.default,
            },
          },
        },
      },
      MuiFormHelperText: {
        styleOverrides: {
          root: {
            fontSize: 10,
            marginBottom: "8px",
          },
        },
      },
      MuiButtonBase: {
        styleOverrides: {
          root: {
            textTransform: "none",
          },
        },
      },
      MuiButtonGroup: {
        styleOverrides: {
          root: {
            textTransform: "none",
          },
        },
      },
      MuiTypography: {
        variants: [
          {
            props: { variant: "label" },
            style: {
              fontSize: 14,
              fontWeight: 500,
              color: "rgba(0,0,0,0.6)",
              marginBottom: "5px",
              display: "inline-block",
              marginTop: "16px",
            },
          },
          {
            props: { variant: "title" },
            style: {
              fontSize: 24,
              fontWeight: 900,
              [themePalette.breakpoints.down("sm")]: {
                fontSize: 18,
              },
            },
          },
          {
            props: {
              variant: "hint",
            },
            style: {
              color: themePalette.palette.text.gray,
            },
          },
          {
            props: {
              variant: "primary",
            },
            style: {
              color: themePalette.palette.text.black,
            },
          },
          {
            props: {
              variant: "secondary",
            },
            style: {
              color: themePalette.palette.text.white,
            },
          },
        ],
      },
      MuiLink: {
        variants: [
          {
            props: {
              variant: "inherit",
            },
            style: {
              color: "inherit",
            },
          },
        ],
        styleOverrides: {
          root: {
            color: "inherit",
            textDecoration: "none",
          },
        },
      },
    },
  },
  ruRU
);

export default theme;
