import { createTheme } from "@mui/material/styles";

import { ruRU } from "@mui/material/locale";

declare module "@mui/material/Chip" {
  interface ChipPropsVariantOverrides {
    rounded: true;
  }
}
declare module "@mui/material/Typography" {
  interface TypographyPropsVariantOverrides {
    label: true;
    title: true;
    pageTitle: true;
    hint: true;
  }
}
declare module "@mui/material/styles" {
  interface Palette {
    border: Palette["primary"];
    button: Palette["primary"];
    input: PaletteOptions["input"];
    bg: PaletteOptions["bg"];
    tg: PaletteOptions["tg"];
  }
  interface PaletteOptions {
    border: PaletteOptions["primary"];
    button: PaletteOptions["primary"];
    input: TypeInput;
    bg?: TypeBackgroundCustom;
    tg?: TypeTgCustom;
  }

  interface TypeText {
    gray: string;
    white: string;
    black: string;
    placeholder: string;
  }

  interface PaletteColor {
    background: string;
  }

  interface SimplePaletteColorOptions {
    background?: string;
    text?: string;
  }

  interface TypeBackgroundCustom {
    default?: string;
    paper?: string;
    header?: string;
  }

  interface TypeTgCustom {
    secondaryBgColor?: string;
    bgColor?: string;
    textColor?: string;
    hintColor?: string;
    linkColor?: string;
    buttonTextColor?: string;
    buttonColor?: string;
  }

  interface TypeInput {
    background: {
      default: string;
      hover: string;
    };
    border?: {
      default: string;
      hover: string;
    };
    text: {
      default: string;
      hover: string;
      black: string;
    };
    placeholder: string;
  }
}
declare module "@mui/material/Button" {
  interface ButtonPropsVariantOverrides {
    light: true;
  }
  interface ButtonPropsColorOverrides {
    button: true;
  }
}

declare module "@mui/material/IconButton" {
  interface IconButtonPropsColorOverrides {
    back: true;
    secondaryFilled: true;
  }
  interface IconButtonPropsSizeOverrides {
    xs: true;
  }
}

declare module "@mui/material/SvgIcon" {
  interface SvgIconPropsColorOverrides {
    blue: true;
    text: true;
    gray: true;
  }
}

declare module "@mui/material/AppBar" {
  interface AppBarPropsColorOverrides {
    white: true;
    black: true;
  }
}

declare module "@mui/material/Paper" {
  interface PaperPropsVariantOverrides {
    simple: true;
    item: true;
    small: true;
  }
}

//@ts-ignore
export const tg = global?.Telegram?.WebApp ?? null;

const themePaletteTG = createTheme({
  palette: {
    button: {
      main: tg?.themeParams.button_color ?? "inherit",
      text: tg?.themeParams.button_text_color ?? "#000",
      light: "#E3E8EB",
      dark: "#000000",
      contrastText: "#fff",
    },
    background: {
      default: "#F7F7F8",
      paper: tg?.themeParams.bg_color ?? "#000",
    },
    primary: {
      main: tg?.themeParams.bg_color ?? "#101418",
    },
    secondary: {
      main: "#31D888",
      background: "#F7FEFA",
    },
    input: {
      background: {
        default: "#FFFFFF",
        hover: "#FFFFFF",
      },
      text: {
        default: "#090A0D",
        hover: "#090A0D",
        black: "#000000",
      },
      border: {
        default: "#EAECF0",
        hover: "#007bff",
      },
      placeholder: "#667085",
    },
    text: {
      primary: tg?.themeParams.text_color ?? "#fff",
      secondary: tg?.themeParams.hint_color ?? "#878787",
      gray: tg?.themeParams.hint_color ?? "#828282",
      white: tg?.themeParams.text_color ?? "#fff",
      black: tg?.themeParams.text_color ?? "#000",
    },
    border: {
      main: "#E2E2E2",
    },
    tg: {
      secondaryBgColor: tg?.themeParams.secondary_bg_color,
      bgColor: tg?.themeParams.bg_color,
      textColor: tg?.themeParams.text_color,
      hintColor: tg?.themeParams.hint_color,
      linkColor: tg?.themeParams.link_color,
      buttonTextColor: tg?.themeParams.button_text_color,
      buttonColor: tg?.themeParams.button_color,
    },
  },
});
// Create a theme instance.
const themeTG = createTheme(
  {
    ...themePaletteTG,

    breakpoints: {
      values: {
        xs: 0,
        sm: 700,
        md: 1000,
        lg: 1300,
        xl: 1520,
      },
    },
    components: {
      MuiButton: {
        variants: [
          {
            props: {
              variant: "contained",
            },
            style: {},
          },
        ],
        styleOverrides: {
          root: {
            color: themePaletteTG.palette.text.white,
          },
        },
      },
      MuiTabs: {
        styleOverrides: {
          root: {
            "& .MuiTabs-indicator": {
              backgroundColor: themePaletteTG.palette.text.primary,
            },
          },
        },
      },
      MuiTab: {
        styleOverrides: {
          root: {
            textTransform: "none",
            "&.Mui-selected": {
              color: themePaletteTG.palette.text.primary,
            },
          },
        },
      },
      MuiToggleButton: {
        styleOverrides: {
          root: {
            textTransform: "none",
            border: "none",
            backgroundColor: themePaletteTG.palette.tg?.secondaryBgColor,
            color: themePaletteTG.palette.tg?.textColor,
            "&.Mui-selected": {
              backgroundColor: "#fff",
              color: "#000",
              ":hover": {
                backgroundColor: themePaletteTG.palette.tg?.secondaryBgColor,
              },
            },
            ":hover": {
              backgroundColor: themePaletteTG.palette.tg?.secondaryBgColor,
              color: themePaletteTG.palette.tg?.textColor,
            },
          },
        },
      },
      MuiFormControl: {
        styleOverrides: {
          root: {
            marginBottom: "16px",
            "& .MuiInputLabel-root": {
              fontSize: "16px",
            },
          },
        },
      },
      MuiInputBase: {
        styleOverrides: {
          root: {
            "&.MuiInput-root": {
              ".MuiInputAdornment-positionStart": {
                paddingLeft: "12px",
                marginRight: "0",
              },
              ".MuiInputAdornment-positionEnd": {
                paddingRight: "20px",
                marginLeft: "4px",
              },
            },
            "&.MuiOutlinedInput-root": {
              borderRadius: "8px",
            },
            fontSize: 16,
            fontWeight: 400,
            border: `1px solid ${themePaletteTG.palette.border.main}`,
            backgroundColor: "#fff",
            borderRadius: "8px",
            color: themePaletteTG.palette.input.text.black,
            "input::placeholder": {
              color: themePaletteTG.palette.input.placeholder,
            },
            "&.MuiAutocomplete-inputRoot": {
              ".MuiAutocomplete-input": {
                padding: 0,
                borderRadius: "8px",
              },
              padding: "10px 14px",
              borderRadius: "8px",
              borderWidth: 0,
              ".MuiOutlinedInput-notchedOutline": {
                border: `1px solid ${themePaletteTG.palette.border.main}`,
              },
            },
            "&.Mui-focused.MuiAutocomplete-inputRoot": {
              ".MuiOutlinedInput-notchedOutline": {
                borderWidth: "2px !important",
              },
            },
            "&.MuiInputBase-formControl .MuiSelect-nativeInput+.MuiSvgIcon-root":
              {
                marginRight: 16,
              },

            "&.MuiInput-input:focus": {
              borderRadius: "8px",
            },
            "&.Mui-focused": {
              ".MuiSelect-select": {
                backgroundColor: "#fff",
              },
              ".MuiOutlinedInput-notchedOutline": {
                borderWidth: "0 !important",
              },
              borderColor: themePaletteTG.palette.input.border?.hover,
              boxShadow: `0px 0px 0px 3px ${themePaletteTG.palette.input.border?.hover}14`,
            },

            "& .MuiInputBase-input": {
              padding: "10px 14px",
              borderRadius: "8px",
              ":focus": {
                borderRadius: "8px",
              },
            },
            ".MuiOutlinedInput-notchedOutline": {
              borderRadius: "8px",
              borderWidth: 0,
            },
            "&.Mui-disabled": {
              ".MuiOutlinedInput-notchedOutline": {
                borderRadius: "8px",
                // border: "initial",
              },
              backgroundColor: themePaletteTG.palette.input.background.default,
            },
          },
        },
      },
      MuiButtonBase: {
        styleOverrides: {
          root: {
            textTransform: "none",
          },
        },
      },
      MuiButtonGroup: {
        styleOverrides: {
          root: {
            textTransform: "none",
          },
        },
      },
      MuiTypography: {
        variants: [
          {
            props: {
              variant: "hint",
            },
            style: {
              color: themePaletteTG.palette.text.gray,
            },
          },
        ],
        styleOverrides: {
          root: {
            color: themePaletteTG.palette.tg?.textColor,
          },
        },
      },
      MuiLink: {
        variants: [
          {
            props: {
              variant: "inherit",
            },
            style: {
              color: "inherit",
            },
          },
        ],
      },
    },
  },
  ruRU
);

export default themeTG;
