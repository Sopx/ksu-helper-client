import { IAnyType, SnapshotIn, types } from "mobx-state-tree";

const NUMBER_OR_STRING = types.union(types.string, types.number);

export const NULLABLE_UNION_STRING_NUMBER = types.maybeNull(NUMBER_OR_STRING);

export const NULLABLE_BOOLEAN = types.maybeNull(types.boolean);
export const NULLABLE_STRING = types.maybeNull(types.string);
export const STRING_ARRAY = types.array(types.string);
export const NULLABLE_NUMBER = types.maybeNull(types.number);
export const PARSED_NUMBER = types.optional(types.number, 0, [null, undefined]);
export const PARSED_BOOLEAN = types.optional(types.boolean, false, [
  null,
  undefined,
]);

export const OPTIONAL_STRING = types.optional(types.string, "", [
  null,
  undefined,
]);
export const OPTIONAL_BOOLEAN = types.optional(types.boolean, false, [
  null,
  undefined,
]);

export const NULLABLE_ARRAY = <T extends IAnyType>(
  model: T,
  defaultValue: SnapshotIn<T>[] = []
) => types.optional(types.array(model), defaultValue, [null, undefined]);

export const STRING_OR_NUMBER = types.optional(
  types.union(types.string, types.number),
  "",
  [null, undefined]
);
