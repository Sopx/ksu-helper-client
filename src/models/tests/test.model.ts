import { Instance, SnapshotIn, flow, types } from "mobx-state-tree";
import { BaseModel } from "../base/base.model";
import {
  NULLABLE_ARRAY,
  NULLABLE_NUMBER,
  NULLABLE_STRING,
} from "../mst-types/mst-types";
import { TestQuestionModel } from "./test-question.model";
import { modalStore } from "../modalStore.model";
import { apiClient } from "@/services/api_client";
import withApi from "../extensions/withApi";
import NotificationService from "@/services/notification_service";
import { rootStore } from "../rootStore.model";

export enum TestPrivacy {
  PUBLIC = "public",
  PRIVATE = "private",
}

export const TestPrivacyArray = Object.values(TestPrivacy);

export const TestModel = BaseModel.named("TestModel")
  .props({
    name: NULLABLE_STRING,
    privacy: types.enumeration(TestPrivacyArray),
    authorId: NULLABLE_STRING,
    questionsCount: NULLABLE_NUMBER,
    questions: NULLABLE_ARRAY(TestQuestionModel),
  })
  .actions((self) => ({
    deleteTest: flow(function* () {
      modalStore.confirmation.show({
        title: "Удаление теста",
        subtitle: "Вы уверены, что хотите удалить тест?",
        onOk: async () => {
          await apiClient.deleteTest(self.id!);
          NotificationService.snackbar.enqueueSnackbar("Тест успешно удален", {
            variant: "success",
          });
          rootStore.tests.refresh();
        },
      });
    }),
  }));

export interface ITestModel extends Instance<typeof TestModel> {}
export interface ITestModelSnapshotIn extends SnapshotIn<typeof TestModel> {}
