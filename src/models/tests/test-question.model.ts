import { Instance, SnapshotIn, types } from "mobx-state-tree";
import { BaseModel } from "../base/base.model";
import {
  NULLABLE_ARRAY,
  NULLABLE_NUMBER,
  NULLABLE_STRING,
} from "../mst-types/mst-types";

export const TestQuestionAnswerModel = BaseModel.named(
  "TestQuestionAnswerModel"
).props({
  value: NULLABLE_STRING,
  id: NULLABLE_STRING,
});

export interface ITestQuestionAnswerModel
  extends Instance<typeof TestQuestionAnswerModel> {}

export const TestQuestionModel = BaseModel.named("TestQuestionModel").props({
  value: NULLABLE_STRING,
  answers: NULLABLE_ARRAY(TestQuestionAnswerModel),
});

export interface ITestQuestionModel
  extends Instance<typeof TestQuestionModel> {}
export interface ITestQuestionModelSnapshotIn
  extends SnapshotIn<typeof TestQuestionModel> {}
