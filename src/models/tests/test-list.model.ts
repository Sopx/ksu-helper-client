import { Instance } from "mobx-state-tree";
import { QueryModel, createListModel } from "../base/base-list.model";
import { TestModel } from "./test.model";
import { NULLABLE_STRING } from "../mst-types/mst-types";

export const TestListQueryModel = QueryModel.props({
  authorId: NULLABLE_STRING,
  searchText: NULLABLE_STRING,
});

export const TestListModel = createListModel(
  "/tests",
  "TestList",
  TestModel,
  TestListQueryModel
);

export interface ITestListModel extends Instance<typeof TestListModel> {}
