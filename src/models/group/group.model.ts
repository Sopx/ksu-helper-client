import { Instance, SnapshotIn, types } from "mobx-state-tree";
import { BaseModel } from "../base/base.model";
import { NULLABLE_ARRAY, NULLABLE_STRING } from "../mst-types/mst-types";
import { UserModel } from "../users/user.model";

export enum GroupTypesEnum {
  PUBLIC = "public",
  PRIVATE = "private",
}

export const GroupTypesEnumArray = Object.values(GroupTypesEnum);

export const GroupModel = BaseModel.named("Group").props({
  name: NULLABLE_STRING,
  owner: types.maybeNull(UserModel),
  members: NULLABLE_ARRAY(types.maybeNull(UserModel)),
  joinRequests: NULLABLE_ARRAY(types.maybeNull(UserModel)),
  admins: NULLABLE_ARRAY(types.maybeNull(UserModel)),
  type: types.enumeration(GroupTypesEnumArray),
});

export interface IGroup extends Instance<typeof GroupModel> {}
export interface IGroupSnapshotIn extends SnapshotIn<typeof GroupModel> {}
