import { getEnv } from "mobx-state-tree";

const withAppTranslate = <T>(self: T) => ({
  views: {
    get t(): (text: string, params?: any) => string {
      return getEnv(this).t;
    },
  },
});
export default withAppTranslate;
