import { IAnyModelType, IModelType } from "mobx-state-tree";

export const withArrayFunctions = <T>(self: T) => ({
  actions: {
    addToArray: (name: string, value = {}) => {
      // @ts-ignore
      self[name].push(value);
    },
    removeFromArray: (name: string, index: number) => {
      // @ts-ignore
      self[name].splice(index, 1);
    },
  },
});
