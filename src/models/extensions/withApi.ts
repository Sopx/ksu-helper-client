import { ApiClientBase } from "@/services/api_client";
import { getEnv } from "mobx-state-tree";

const withApi = <T>(self: T) => ({
  views: {
    get api(): ApiClientBase {
      return getEnv(this).api;
    },
  },
});
export default withApi;
