import { getEnv } from "mobx-state-tree";
import { Router } from "next/router";

const withRouter = <T>(self: T) => ({
  views: {
    get router(): Router {
      return getEnv(this).router;
    },
  },
});
export default withRouter;
