import { getEnv } from "mobx-state-tree";
import { ProviderContext } from "notistack";

const withSnackbar = <T>(self: T) => ({
  views: {
    get snackbar(): ProviderContext {
      return getEnv(this).snackbar;
    },
  },
});
export default withSnackbar;
