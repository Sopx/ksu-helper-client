import { getEnv } from "mobx-state-tree";

import { IRootStore } from "./withRootStore";

const withRootStoreEnv = <T>(self: T) => ({
  views: {
    get rootStore(): IRootStore {
      return getEnv(this).rootStore;
    },
  },
});
export default withRootStoreEnv;
