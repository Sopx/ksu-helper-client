import { getRoot } from "mobx-state-tree";
import { IUser } from "../users/user.model";

export interface IRootStore {
  user: IUser;
}

const withRootStore = <T>(self: T) => ({
  views: {
    get rootStore(): IRootStore {
      return getRoot(self);
    },
  },
});
export default withRootStore;
