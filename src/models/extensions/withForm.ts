import { applyPatch, getSnapshot, SnapshotOut } from "mobx-state-tree";

/**
 * Adds a rootStore property to the node for a convenient
 * and strongly typed way for stores to access other stores.
 */

const withForm = <T>(self: T) => ({
  actions: {
    //{name:{addres:{s:1}}}
    onChange: (value: any, name: any) => {
      applyPatch(self, {
        path: `/${name}`,
        value,
        op: "replace",
      });
    },
  },
  views: {
    get json(): SnapshotOut<T> {
      return getSnapshot(self as any);
    },
  },
});
export default withForm;
