import {
  cast,
  destroy,
  flow,
  getSnapshot,
  IAnyType,
  Instance,
  SnapshotIn,
  SnapshotOrInstance,
  types,
} from "mobx-state-tree";
import withApi from "../extensions/withApi";
import withRouter from "../extensions/withRouter";

export interface IBasePaginated<T> {
  docs: T[];
  totalDocs: number;
  totalPages: number;
  page: number;
  limit: number;
}

export const QueryModel = types
  .model("Query")
  .props({
    page: types.optional(types.union(types.string, types.number), 1),
    limit: types.optional(types.union(types.string, types.number), 10),
  })
  .views((self) => ({
    get json() {
      return getSnapshot(self);
    },
  }));

export const createListModel = <
  T extends IAnyType,
  Q extends typeof QueryModel
>(
  url: string,
  name: string,
  model: T,
  query: Q,
  method: "GET" | "POST" = "GET"
) => {
  return types
    .model(`BaseList${name}`)
    .props({
      url: url,
      items: types.array(model),
      loading: true,
      query: types.optional(query, {}),
      method,
      totalPages: types.optional(types.number, 1),
      totalDocs: types.optional(types.number, 0),
    })
    .extend(withApi)
    .extend(withRouter)
    .views((self) => ({
      get queryJSON() {
        return getSnapshot(self.query);
      },
    }))
    .actions((self) => ({
      setQuery: (value: any) => {
        self.query = value;
      },
      getItems: flow(function* getItems(
        params?: SnapshotIn<Q>,
        checkExistance = false
      ) {
        try {
          if (checkExistance && self.items.length) {
            return;
          }
          self.loading = true;
          let _params = getSnapshot(query.create(params));
          if (!params || Object.keys(params).length === 0) {
            _params = self.query;
          }

          let request;
          if (self.method === "GET") {
            request = self.api.get<IBasePaginated<T>>(self.url, {
              params: _params,
            });
          } else {
            request = self.api.post<IBasePaginated<T>>(self.url, _params);
          }
          const { data } = yield request;
          self.items = data.docs ?? data;

          self.query = cast(_params);
          self.query.page = data.page || 1;
          self.totalPages = data?.totalPages ?? 1;
          self.totalDocs = data?.totalDocs ?? 1;
        } catch (error) {
          console.log(error);
        } finally {
          self.loading = false;
        }
      }),
    }))
    .actions((self) => ({
      refresh: () => {
        // @ts-ignore
        self.getItems(getSnapshot(self.query));
      },
      onChange: (
        pagination: IQuerySnapshotIn = { limit: 10, page: 1 },
        filters: any = {}
      ) => {
        const { query: routerQuery = {} } = self.router;
        self.query = {
          ...routerQuery,
          ...filters,
          ...pagination,
        };

        // @ts-ignore
        self.router.push({ query: self.queryJSON });
      },
      addItems: (items: SnapshotOrInstance<T>[]) => {
        self.items.push(...items);
      },
      insertItems: (items: SnapshotOrInstance<T>[]) => {
        self.items.unshift(...items);
      },
      deleteItem: (item: Instance<T>) => {
        destroy(item);
      },
      editItem: (id: string, key: any, value: any) => {
        const item = self.items.find((i) => i.id === id);
        if (item) item[key] = value;
      },
      updateAllItems: (key: any, value: any) => {
        self.items.forEach((i) => {
          i[key] = value;
        });
      },
      loadMore: flow(function* () {
        if (!self.loading) {
          try {
            self.loading = true;
            const page = parseInt(self.query.page as string) + 1;
            // @ts-ignore
            const items = yield self.api.get<IBasePaginated<T>>(self.url, {
              params: { ...self.query, page },
            });

            if (items.data.docs.length) {
              self.items = cast([...self.items, ...items.data.docs]);
              self.query.page = page;
            }
          } catch (error) {
            console.log(error);
          } finally {
            self.loading = false;
          }
        }
      }),
    }));
};

export interface IQuerySnapshotIn extends SnapshotIn<typeof QueryModel> {}
