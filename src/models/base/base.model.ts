import { types } from "mobx-state-tree";
import { NULLABLE_STRING, OPTIONAL_BOOLEAN } from "../mst-types/mst-types";

export const BaseModel = types.model("Base").props({
  createdAt: NULLABLE_STRING,
  updatedAt: NULLABLE_STRING,
  id: NULLABLE_STRING,
  loading: OPTIONAL_BOOLEAN,
  isDeleted: OPTIONAL_BOOLEAN,
});
