import { Instance, SnapshotIn, types } from "mobx-state-tree";
import { BaseModel } from "../base/base.model";
import { NULLABLE_ARRAY, NULLABLE_STRING } from "../mst-types/mst-types";
import { GroupModel } from "../group/group.model";

export enum UserRoleEnum {
  user = "User",
  admin = "Admin",
}

export enum GroupUserRoleEnum {
  owner = "owner",
  member = "member",
  admin = "admin",
}

export const UserRoleEnumArray = Object.values(UserRoleEnum);
export const GroupUserRoleEnumArray = Object.values(GroupUserRoleEnum);

export const UserModel = BaseModel.named("User").props({
  email: NULLABLE_STRING,
  roles: types.array(types.enumeration(UserRoleEnumArray)),
  group: NULLABLE_STRING,
  groupRole: types.maybeNull(types.enumeration(GroupUserRoleEnumArray)),
  notifications: NULLABLE_ARRAY(NULLABLE_STRING),
});

export interface IUser extends Instance<typeof UserModel> {}
export interface IUserSnapshotIn extends SnapshotIn<typeof UserModel> {}
