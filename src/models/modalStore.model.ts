import { Instance, types } from "mobx-state-tree";
import { NULLABLE_NUMBER } from "./mst-types/mst-types";

const BaseModalModel = types
  .model("BaseModal")
  .props({
    visible: types.optional(types.boolean, false),
  })
  .actions((self) => {
    let resolve = (v: any) => {};
    return {
      show: () => {
        self.visible = true;
        return new Promise((r) => (resolve = r));
      },
      hide: () => {
        self.visible = false;
        resolve(true);
      },
    };
  });

const ChangePasswordModal = BaseModalModel.named("ChangePasswordModal")
  .props({ closable: types.optional(types.boolean, true) })
  .actions((self) => {
    let resolve = (v: any) => {};
    return {
      show: (closable = true) => {
        self.visible = true;
        self.closable = closable;
        return new Promise((r) => (resolve = r));
      },
      hide: () => {
        self.visible = false;
        resolve(true);
      },
    };
  });

const ConfirmationModal = BaseModalModel.named("ConfirmationModal")
  .props({
    title: types.optional(types.string, "Подтверждение"),
    subtitle: types.optional(types.string, ""),
    okText: types.optional(types.string, "Продолжить"),
    showCancel: types.optional(types.boolean, true),
    showTooltip: types.optional(types.boolean, false),
    tooltipTitle: types.optional(types.string, ""),
    tooltipSubtitle: types.optional(types.string, ""),
  })
  .actions((self) => {
    let _onOk = () => {};
    return {
      show: ({
        title = "Подтверждение",
        subtitle = "",
        onOk,
        okText = "Продолжить",
        showCancel = true,
        showTooltip = false,
        tooltipTitle = "",
        tooltipSubtitle = "",
      }: {
        title?: string;
        subtitle?: string;
        onOk?: VoidFunction;
        okText?: string;
        showCancel?: boolean;
        showTooltip?: boolean;
        tooltipTitle?: string;
        tooltipSubtitle?: string;
      }) => {
        _onOk = onOk ?? _onOk;
        self.visible = showCancel;
        self.title = title;
        self.subtitle = subtitle;
        self.okText = okText;
        self.showTooltip = showTooltip;
        self.tooltipTitle = tooltipTitle;
        self.tooltipSubtitle = tooltipSubtitle;
        self.showCancel = true;
      },
      onOk: () => {
        self.hide();
        _onOk();
      },
    };
  });

const LoaderModal = BaseModalModel.named("LoaderModal")
  .props({
    text: types.optional(types.string, ""),
    progress: NULLABLE_NUMBER,
  })
  .actions((self) => {
    let resolve = (v: any) => {};
    return {
      show: (text = "") => {
        self.visible = true;
        self.text = text;
        self.progress = null;
        return new Promise((r) => (resolve = r));
      },
      hide: () => {
        self.progress = null;
        self.visible = false;
        resolve(true);
      },
      setProgress: (value?: number) => {
        self.progress = value ? (value >= 100 ? null : value) : null;
      },
    };
  });

const PaymentInformationModal = BaseModalModel.named("PaymentInformationModal")
  .props({
    phone: types.optional(types.string, "77777303932"),
  })
  .actions((self) => ({
    setPhone: (value?: string) => {
      self.phone = value ?? "77777303932";
    },
  }));

const ModalStoreModel = types.model("ModalStore").props({
  resetPassword: types.optional(BaseModalModel.named("ResetPasswordModal"), {}),
  changePassword: types.optional(ChangePasswordModal, {}),
  loader: types.optional(LoaderModal, {}),
  confirmation: types.optional(ConfirmationModal, {}),
  paymentInformation: types.optional(PaymentInformationModal, {}),
  limitsError: types.optional(BaseModalModel.named("LimitsErrortModal"), {}),
});

export interface IModalStore extends Instance<typeof ModalStoreModel> {}

export const modalStore = ModalStoreModel.create({});
