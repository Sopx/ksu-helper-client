import { Instance, SnapshotIn, cast, flow, types } from "mobx-state-tree";
import { IUserSnapshotIn, UserModel } from "./users/user.model";
import { apiClient } from "@/services/api_client";
import { NULLABLE_BOOLEAN } from "./mst-types/mst-types";
import withApi from "./extensions/withApi";
import { TestListModel } from "./tests/test-list.model";

export const RootStoreModel = types
  .model("RootStore")
  .props({
    user: types.maybeNull(UserModel),
    tests: types.optional(TestListModel, {}),
    isAuth: NULLABLE_BOOLEAN,
  })
  .extend(withApi)
  .actions((self) => ({
    setUser: flow(function* (value: IUserSnapshotIn) {
      self.user = cast(value);
      self.isAuth = value ? true : false;
    }),
    getProfile: flow(function* () {
      try {
        const { data } = yield self.api.getProfile();
        self.user = cast(data);
        self.isAuth = data ? true : false;
      } catch (error) {
        console.log(error);
        throw error;
      }
    }),
  }));

export interface IRootStore extends Instance<typeof RootStoreModel> {}
export interface IRootStoreSnapshotIn
  extends SnapshotIn<typeof RootStoreModel> {}

export let rootStore: IRootStore;

export function initRootStore(params: Partial<IRootStoreSnapshotIn>, t: any) {
  if (!rootStore) {
    rootStore = RootStoreModel.create(params, { api: apiClient, t });
  } else if (params.user) {
    rootStore.setUser(params.user);
  }
}
