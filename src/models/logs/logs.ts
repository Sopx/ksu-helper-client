export interface logsItemInterface {
  level: string;
  message: string;
  meta: any;
  timestamp: string;
  _id: string;
}

export interface logsInterface {
  page: number;
  limit: number;
  documents: logsItemInterface[];
  desiredLogLevels: [];
  totalPages: number;
}

export const logsStatusLevel = {
  SILLY: "silly",
  ERROR: "error",
  WARN: "warn",
  INFO: "info",
  VERBOSE: "verbose",
  DEBUG: "debug",
};

export interface LogsBotStatusHandlerParamsInterface {
  levels: string[];
  limit: number;
}

export const logsStatusLevelArray = [
  "error",
  "warn",
  "info",
  "verbose",
  "debug",
  "silly",
];
