import {GetServerSidePropsContextApi} from "@/lib/withSessionSsr";

export default function Page() {
  return null;
}

export const getServerSideProps = async ({
  props,
}: GetServerSidePropsContextApi) => {
  return {
    props: {...props},
    redirect: {
      destination:
        "https://chromewebstore.google.com/detail/tolyan-assistant/kmklpinmghmgfppaaccjkjgmoifffele",
      permanent: false,
    },
  };
};
