import { Inter } from "next/font/google";
import { withSessionSsr } from "@/lib/withSessionSsr";

export default function Home() {
  return <></>;
}

export const getServerSideProps = () => {
  return { redirect: { destination: "/main" } };
};
