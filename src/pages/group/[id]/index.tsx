import AppLayout from "@/components/AppLayout";
import { withSessionSsr } from "@/lib/withSessionSsr";
import { IGroup } from "@/models/group/group.model";
import { rootStore } from "@/models/rootStore.model";
import { GroupUserRoleEnum } from "@/models/users/user.model";
import { apiClient } from "@/services/api_client";
import { useAppTranslation } from "@/services/i18n";
import NotificationService from "@/services/notification_service";
import {
  Button,
  Card,
  CardContent,
  CardHeader,
  Stack,
  Typography,
} from "@mui/material";
import { observer } from "mobx-react";
import { useRouter } from "next/router";
import { useCallback } from "react";

export default observer((props: any) => {
  const { group }: { group: IGroup } = props;
  const { t } = useAppTranslation();
  const router = useRouter();

  const handleDelete = useCallback(async () => {
    try {
      await apiClient.removeGroup(group.id!);
      NotificationService.snackbar?.enqueueSnackbar("Группа успешно удалена", {
        variant: "success",
      });
      router.replace("/group");
    } catch (error) {
      console.log(error);
    }
  }, [group.id, router]);

  return (
    <AppLayout>
      <Card variant="fullpage">
        <CardHeader
          title={
            <Stack
              direction={"row"}
              alignItems="center"
              justifyContent="space-between"
            >
              <Typography variant="h6">{group.name}</Typography>
              {rootStore.user?.groupRole === GroupUserRoleEnum.owner && (
                <Button variant="contained" onClick={handleDelete}>
                  Удалить группу
                </Button>
              )}
            </Stack>
          }
        />
        <CardContent>
          <Stack>
            <Stack
              direction="row"
              alignItems={"center"}
              justifyContent={"space-between"}
            >
              <Typography variant="subtitle1">Группа: {group.name}</Typography>
              <Typography variant="body2">
                Кол-во участников: {group.members.length}
              </Typography>
            </Stack>
            <Stack>
              Участники:
              {group.members.map((member, i: number) => (
                <Stack
                  key={i}
                  direction="row"
                  alignItems={"center"}
                  justifyContent={"space-between"}
                >
                  <Typography variant="body2">{member?.email}</Typography>
                  {/* <Typography variant="body2">{member?.groupRole}</Typography> */}
                </Stack>
              ))}
            </Stack>
          </Stack>
        </CardContent>
      </Card>
    </AppLayout>
  );
});

export const getServerSideProps = withSessionSsr(
  ["form"],
  async ({ props, params, api }) => {
    const { data } = await api.getGroupById(params?.id as string);
    console.log(data);
    return { props: { group: data, ...props } };
  }
);
