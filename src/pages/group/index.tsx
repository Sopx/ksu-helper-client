import AppLayout from "@/components/AppLayout";
import { withSessionSsr } from "@/lib/withSessionSsr";
import { IGroup } from "@/models/group/group.model";
import { rootStore } from "@/models/rootStore.model";
import { useAppTranslation } from "@/services/i18n";
import {
  Button,
  Card,
  CardContent,
  CardHeader,
  Stack,
  Typography,
} from "@mui/material";
import { observer } from "mobx-react";
import { useRouter } from "next/router";
import { useCallback } from "react";

export default observer((props: any) => {
  const { group }: { group: IGroup } = props;
  const { t } = useAppTranslation();
  const router = useRouter();
  const handleCreate = useCallback(() => {
    router.push("/group/create");
  }, [router]);

  return (
    <AppLayout>
      <Card variant="fullpage">
        <CardHeader
          title={
            <Stack
              direction={"row"}
              alignItems="center"
              justifyContent="space-between"
            >
              <Typography variant="h6">{t("Моя Группа")}</Typography>
              {!group && (
                <Button variant="contained" onClick={handleCreate}>
                  Создать группу
                </Button>
              )}
            </Stack>
          }
        />
        <CardContent>
          {!!group ? (
            <Stack>
              <Stack
                direction="row"
                alignItems={"center"}
                justifyContent={"space-between"}
              >
                <Typography variant="subtitle1">
                  Группа: {group.name}
                </Typography>
                <Typography variant="body2">
                  Кол-во участников: {group.members.length}
                </Typography>
              </Stack>
              <Stack>
                Участники:
                {group.members.map((member, i: number) => (
                  <Stack
                    key={i}
                    direction="row"
                    alignItems={"center"}
                    justifyContent={"space-between"}
                  >
                    <Typography variant="body2">{member?.email}</Typography>
                    {/* <Typography variant="body2">{member?.groupRole}</Typography> */}
                  </Stack>
                ))}
              </Stack>
            </Stack>
          ) : (
            <Stack flex={1} alignItems={"center"} justifyContent={"center"}>
              <Typography variant="body2" color="text.gray">
                Нема у вас группы...
              </Typography>
            </Stack>
          )}
        </CardContent>
      </Card>
    </AppLayout>
  );
});

export const getServerSideProps = withSessionSsr(["form"], async (context) => {
  return { props: context.props };
});
