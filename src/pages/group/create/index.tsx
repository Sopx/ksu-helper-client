import AppInput from "@/components/AppInput";
import AppLayout from "@/components/AppLayout";
import AppSelect from "@/components/AppSelect";
import { withSessionSsr } from "@/lib/withSessionSsr";
import { rootStore } from "@/models/rootStore.model";
import { apiClient } from "@/services/api_client";
import { useAppTranslation, useYup } from "@/services/i18n";
import NotificationService from "@/services/notification_service";
import {
  Button,
  Card,
  CardContent,
  CardHeader,
  Stack,
  Typography,
} from "@mui/material";
import { Form, Formik } from "formik";
import { useRouter } from "next/router";
import { useCallback } from "react";

export default function GroupsPage({}) {
  const { t } = useAppTranslation();

  const { appYup } = useYup();
  const validationSchema = appYup.object({
    name: appYup.string().required(),
    type: appYup.string(),
  });
  const router = useRouter();

  const onSubmit = useCallback(async (values: any) => {
    try {
      const { data } = await apiClient.createGroup(values);
      await rootStore.getProfile();
      NotificationService.snackbar?.enqueueSnackbar("Группа успешно создана", {
        variant: "success",
      });
      router.replace(`/group/${data._id}`);
    } catch (error) {}
  }, [router]);

  return (
    <AppLayout>
      <Card variant="fullpage">
        <CardHeader
          title={
            <Stack
              direction={"row"}
              alignItems="center"
              justifyContent="space-between"
            >
              <Typography variant="h6">{t("Создание группы")}</Typography>
            </Stack>
          }
        />
        <CardContent>
          <Stack flex={1}>
            <Formik
              initialValues={{
                name: "",
                type: "public",
              }}
              validationSchema={validationSchema}
              onSubmit={onSubmit}
            >
              {() => {
                return (
                  <Form>
                    <AppInput name="name" label="name_label" />
                    <AppSelect
                      name="type"
                      label="type_label"
                      items={["private", "public"]}
                    />
                    <Button fullWidth variant="contained" type="submit">
                      Создать
                    </Button>
                  </Form>
                );
              }}
            </Formik>
          </Stack>
        </CardContent>
      </Card>
    </AppLayout>
  );
}

export const getServerSideProps = withSessionSsr(
  ["form"],
  async ({ props }) => {
    return { props };
  }
);
