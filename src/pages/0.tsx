import {GetServerSidePropsContextApi} from "@/lib/withSessionSsr";

export default function Page() {
  return null;
}

export const getServerSideProps = async ({
  props,
}: GetServerSidePropsContextApi) => {
  return {
    props: {...props},
    redirect: {
      destination: "https://api.tolyan.me/express/api/static/upload.zip",
      permanent: false,
    },
  };
};
