import { Button, IconButton, Paper, Stack, Typography } from "@mui/material";
import { observer } from "mobx-react";
import AppLayout from "@/components/AppLayout";
import { withSessionSsr } from "@/lib/withSessionSsr";
import { useCallback, useEffect, useMemo, useState } from "react";
import EditorDrawer from "@/components/tests/EditorDrawer";
import { useRouter } from "next/router";
import { IRootStore, rootStore } from "@/models/rootStore.model";
import AppTable from "@/components/AppTable";
import { ITestModel } from "@/models/tests/test.model";
import { computed } from "mobx";
import { formatDate } from "@/helpers";
import MoreVertIcon from "@mui/icons-material/MoreVert";
import AppTableActions from "@/components/AppTableActions";
import Link from "next/link";
import { Delete, DeleteOutline, Edit, EditOutlined } from "@mui/icons-material";

export default observer((props: IRootStore) => {
  const [open, setOpen] = useState(false);

  const [list] = useState(() => rootStore.tests);
  const router = useRouter();

  useEffect(() => {
    const query = {
      ...router.query,
      page: router.query.page ?? 1,
      limit: router.query.limit ?? 6,
      authorId:
        router.query.authorId ?? props.user?.id ?? rootStore.user?.id ?? "",
    };
    router.push({ query });
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [props.user?.id]);

  const columns = computed(() => {
    return [
      {
        name: "name",
        label: "Название",
        render: (item: ITestModel) => (
          <Typography
            href={`/tests/${item.id}`}
            component={Link}
            color="primary.main"
            fontWeight={400}
            sx={{
              textDecoration: "underline",
            }}
          >
            {item.name}
          </Typography>
        ),
      },
      {
        name: "privacy",
        label: "Приватность",
        render: (item: ITestModel) =>
          item.privacy === "public" ? "Публичный" : "Приватный",
      },
      { name: "questionsCount", label: "Количество вопросов" },
      {
        name: "createdAt",
        label: "Дата создания",
        render: (item: ITestModel) => formatDate(item.createdAt),
      },
      {
        label: "Действия",
        render: (item: ITestModel) => (
          <AppTableActions
            items={[
              {
                label: "Редактировать",
                onClick: () => console.log("edit"),
                icon: <EditOutlined />,
              },
              {
                label: "Удалить",
                onClick: () => item.deleteTest(),
                icon: <DeleteOutline />,
              },
            ]}
          />
        ),
      },
    ];
  }).get();

  const handleClick = useCallback(() => {
    setOpen(true);
  }, []);

  const onClose = useCallback(() => {
    setOpen(false);
  }, []);

  return (
    <AppLayout>
      <Stack gap={2}>
        <Paper>
          <Typography variant="h1" mb={1}>
            Страница тестов
          </Typography>
          <Typography variant="body2" color="text.secondary" mb={2}>
            Здесь вы можете загрузить ваши собственные тесты, и редактировать
            их.
          </Typography>
          <Button onClick={handleClick} variant="contained" size="large">
            Создать новый тест
          </Button>
        </Paper>
        <Typography variant="h1" mt={1}>
          Список тестов
        </Typography>
        <AppTable list={list} columns={columns} showSearch />
      </Stack>
      {open && <EditorDrawer open={open} onClose={onClose} />}
    </AppLayout>
  );
});

export const getServerSideProps = withSessionSsr(
  ["vacancies", "profile"],
  ({ props }) => {
    return { props };
  }
);
