import { observer } from "mobx-react";
import AppLayout from "@/components/AppLayout";
import { withSessionSsr } from "@/lib/withSessionSsr";
import { ITestModel } from "@/models/tests/test.model";
import { Paper, Stack, Typography } from "@mui/material";

export default observer(({ testSnapshot }: { testSnapshot: ITestModel }) => {
  return (
    <AppLayout>
      <Typography variant="h1">{testSnapshot.name}</Typography>
      {testSnapshot.questions.map((card, index) => (
        <Paper sx={{ flex: 1, mt: 2 }} key={index}>
          <Stack gap={3} direction="column">
            <Stack direction={"row"} gap={1} alignItems={"start"}>
              <Typography sx={{ display: "inline" }} fontWeight={500}>
                Вопрос {index + 1}:
              </Typography>
              <Typography
                style={{ flex: 1 }}
                dangerouslySetInnerHTML={{
                  __html: card.value!.replaceAll("&lt;question&gt;", ""),
                }}
              />
            </Stack>
            <Stack flex={1} gap={1}>
              {card.answers.map((answer, i: number) => (
                <Stack
                  direction={"row"}
                  alignItems={"center"}
                  gap={1}
                  key={i}
                  sx={{ "& p": { display: "inline" } }}
                >
                  <Typography
                    key={i}
                    sx={{ display: "inline" }}
                    fontWeight={400}
                  >
                    {i + 1}.
                  </Typography>
                  <Typography
                    style={{ display: "inline" }}
                    dangerouslySetInnerHTML={{
                      __html: answer.value!.replaceAll("&lt;variant&gt;", ""),
                    }}
                  />
                </Stack>
              ))}
            </Stack>
          </Stack>
        </Paper>
      ))}
    </AppLayout>
  );
});

export const getServerSideProps = withSessionSsr(
  ["vacancies", "profile"],
  async ({ props, params, api }) => {
    const test = await api.getTestById(params?.id as string);
    return { props: { ...props, testSnapshot: test.data } };
  }
);
