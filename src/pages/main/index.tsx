import { withSessionSsr } from "@/lib/withSessionSsr";
import AppLayout from "@/components/AppLayout";
import { Paper, Stack, Typography } from "@mui/material";
import { useAppTranslation } from "@/services/i18n";

export default function Home() {
  const { t } = useAppTranslation();

  return (
    <>
      <AppLayout>
        <Stack>
          <Paper>
            <Typography variant="h1">{t("main")}</Typography>
          </Paper>
        </Stack>
      </AppLayout>
    </>
  );
}

export const getServerSideProps = withSessionSsr(["vacancies"], ({ props }) => {
  return { props };
});
