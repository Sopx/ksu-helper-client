import { apiClient } from "@/services/api_client";
import { Typography, Stack, Button, Pagination, Paper } from "@mui/material";
import React, { useCallback, useMemo, useState } from "react";

import LogsBotTable from "@/components/logs/bot/LogsBotTable";
import {
  LogsBotStatusHandlerParamsInterface,
  logsInterface,
  logsStatusLevelArray,
} from "@/models/logs/logs";
import LogsBotStatusHandler from "@/components/logs/bot/LogsBotStatusHandler";

export default function BotLogs(logs: logsInterface) {
  const [newLogs, setNewLogs] = useState<logsInterface>();
  const [params, setParams] = useState<LogsBotStatusHandlerParamsInterface>({
    levels: [],
    limit: 50,
  });

  const getLogs = useCallback(
    async (
      page?: number,
      levels?: typeof logsStatusLevelArray,
      limit?: number
    ) => {
      const response = await apiClient.getBotLogs(
        `?limit=${limit}${
          levels?.length ? `&levels=${levels.join(",")}` : ""
        }&page=${page ? page : 1}`
      );

      setNewLogs(response.data);
      return;
    },
    []
  );

  const finishFilters = useCallback(
    async (
      page?: number,
      levels?: typeof logsStatusLevelArray,
      limit?: number
    ) => {
      try {
        await getLogs(page, levels, limit);
      } catch (error) {
        console.log(error);
      }
    },
    [getLogs]
  );

  const handleSetParams = useCallback(
    (levels: typeof logsStatusLevelArray, limit: number) => {
      setParams({ levels, limit });
    },
    []
  );

  const handleOnPageChange = useCallback(
    async (_: any, page: number) => {
      await finishFilters(page, params.levels, params.limit);
    },
    [finishFilters, params]
  );

  return (
    <>
      <Stack
        py={1}
        px={2}
        rowGap={2}
        width={"100%"}
        height={"100%"}
        sx={{
          bgcolor: "#1a1d21",
        }}
      >
        <Stack direction="row" gap={10} alignItems={"center"}>
          <Typography variant="h3">Логи ботика</Typography>
          <LogsBotStatusHandler
            finishFilters={finishFilters}
            handleSetParams={handleSetParams}
          />
          <Paper sx={{ py: 0.5 }}>
            <Pagination
              color={"primary"}
              page={newLogs ? newLogs.page : logs.page}
              count={newLogs ? newLogs.totalPages : logs.totalPages}
              onChange={handleOnPageChange}
            />
          </Paper>
        </Stack>
        <LogsBotTable logs={newLogs ?? logs} />
      </Stack>
    </>
  );
}

export const getServerSideProps = async () => {
  const response = await apiClient.getBotLogs();

  const { data } = response;
  if (!response) {
    return {
      error: "shit",
    };
  }
  return { props: { ...data } };
};
