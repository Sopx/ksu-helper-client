import { apiClient } from "@/services/api_client";
import { useCallback, useEffect, useState } from "react";
import FullSchedule from "@/components/schedule/FullSchedule";
import AppLoadingSpinner from "@/components/AppLoadingSpinner";
import { Box, Stack, Typography, createTheme } from "@mui/material";
import axios from "axios";
import { ThemeProvider } from "@emotion/react";
import themeTG from "@/theme/themeTG";

//@ts-ignore
export default function SchedulePage() {
  const [data, setData] = useState<any>(null);
  const [isLoading, setIsLoading] = useState(true);

  useEffect(() => {
    async function getData() {
      try {
        //@ts-ignore
        const tg = global.Telegram.WebApp ?? window.Telegram.WebApp;
        const id = tg?.initDataUnsafe?.user?.id ?? "1474496868";

        const response = await axios.get(
          `https://api.tolyan.me/bot/get_user_schedule?userId=${id}`
        );
        setData(response.data);
        tg?.expand();
        setIsLoading(false);
      } catch (error) {
        console.log(error);
        setIsLoading(false);
      }
    }
    getData();
  }, []);

  return (
    <ThemeProvider theme={themeTG}>
      {!isLoading && data && <FullSchedule data={data} />}
      {isLoading && !data && <AppLoadingSpinner />}
      {!isLoading && !data && (
        <Stack
          width={"100%"}
          height={"100%"}
          alignItems={"center"}
          justifyContent={"center"}
        >
          <Typography fontSize={26} fontWeight={500}>
            Веб интерфейс Толика уснул, зайдите пожалуйста позже
          </Typography>
        </Stack>
      )}
    </ThemeProvider>
  );
}
