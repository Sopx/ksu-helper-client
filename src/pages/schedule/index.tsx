import AppLoadingSpinner from "@/components/AppLoadingSpinner";
import { useRouter } from "next/router";
import { useEffect } from "react";

export default function SchedulePageRedirect() {
  return <>
    <AppLoadingSpinner />
  </>;
}
