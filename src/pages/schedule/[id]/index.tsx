import AppCalendar from "@/components/schedule/AppCalendar";
import FullSchedule from "@/components/schedule/FullSchedule";
import TableSchedule from "@/components/schedule/TableSchedule";
import { apiClient } from "@/services/api_client";
import {
  Button,
  Container,
  Grid,
  ToggleButton,
  ToggleButtonGroup,
  Typography,
} from "@mui/material";
import { endOfWeek, format, startOfDay, startOfWeek } from "date-fns";
import { useCallback, useState } from "react";

//@ts-ignore
export default function SchedulePage({ returnedData }) {
  return (
    <>
      <FullSchedule data={returnedData} />
    </>
  );
}

export const getServerSideProps = async (context: any) => {
  try {
    const { query } = context;

    const response = await fetch(
      `https://api.tolyan.me/express/api/get_user_schedule?userId=${query.id}`,
      { method: "GET" }
    );

    const data = await response.json();
    if (!response) {
      return {
        error: "shit",
      };
    }
    return { props: { returnedData: data } };
  } catch (error) {
    console.log(error);
    return {
      // redirect: {
      //   destination: "/",
      //   permanent: false,
      // },
    };
  }
};
