import { apiClient } from "@/services/api_client";
import { useAppTranslation, useInitLocale } from "@/services/i18n";
import NotificationService from "@/services/notification_service";
import theme from "@/theme/theme";
import { Close } from "@mui/icons-material";
import { Button, IconButton, ThemeProvider, styled } from "@mui/material";
import { appWithTranslation } from "next-i18next";
import type { AppProps } from "next/app";
import {
  MaterialDesignContent,
  SnackbarProvider,
  useSnackbar,
} from "notistack";
import { ReactElement, ReactNode, useEffect } from "react";
import { useRouter } from "next/dist/client/router";
import { initRootStore } from "@/models/rootStore.model";
import { EmotionCache } from "@emotion/react/macro";
import { NextPage } from "next";
import createCache from "@emotion/cache";
import { CacheProvider } from "@emotion/react";
import "@/styles/globals.css";
import "@/styles/fonts.css";
import { Observer, Provider } from "mobx-react";
import Head from "next/head";
import { modalStore } from "@/models/modalStore.model";
import AppConfirmationModal from "@/components/modals/AppConfirmationModal";

const StyledMaterialDesignContent = styled(MaterialDesignContent)(() => ({
  "&.notistack-MuiContent-success": {
    backgroundColor: theme.palette.primary.main,
  },
  "&.notistack-MuiContent-error": {
    backgroundColor: theme.palette.error.main,
  },
}));

const clientSideEmotionCache = createCache({ key: "css", prepend: true });

interface MyAppProps extends AppProps {
  emotionCache?: EmotionCache;
}

export type NextPageWithLayout = NextPage & {
  getLayout?: (page: ReactElement) => ReactNode;
};

type AppPropsWithLayout = MyAppProps & {
  Component: NextPageWithLayout;
};

function App(props: AppPropsWithLayout) {
  const { Component, emotionCache = clientSideEmotionCache, pageProps } = props;
  const snackbar = useSnackbar();
  const router = useRouter();

  const { t } = useAppTranslation();

  useEffect(() => {
    NotificationService.setSnackbar(snackbar, t);
  }, [snackbar, t]);

  useEffect(() => {
    apiClient.setRouter(router);
  }, [router]);

  return (
    <CacheProvider value={emotionCache}>
      <ThemeProvider theme={theme}>
        <Component {...pageProps} />
        <Observer>
          {() => (
            <>{modalStore.confirmation.visible && <AppConfirmationModal />}</>
          )}
        </Observer>
      </ThemeProvider>
    </CacheProvider>
  );
}
function CloseButton({ id }: any) {
  const snackbar = useSnackbar();

  return (
    <IconButton
      sx={{ color: "white" }}
      onClick={() => {
        snackbar.closeSnackbar(id);
      }}
    >
      <Close />
    </IconButton>
  );
}
function MyApp(props: AppProps) {
  useInitLocale();
  const { t } = useAppTranslation();
  initRootStore({ user: props.pageProps.user }, t);

  return (
    <Provider>
      <Head>
        <title>TolyanMe</title>
        <meta
          name="viewport"
          content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"
        />
      </Head>
      <SnackbarProvider
        Components={{
          success: StyledMaterialDesignContent,
          error: StyledMaterialDesignContent,
        }}
        anchorOrigin={{ horizontal: "left", vertical: "top" }}
        action={(id) => <CloseButton id={id} />}
      >
        <App {...props} />
      </SnackbarProvider>
    </Provider>
  );
}

export default appWithTranslation(MyApp);
