import {
  Box,
  Card,
  CardHeader,
  Container,
  Paper,
  Stack,
  Typography,
} from "@mui/material";
import { observer } from "mobx-react";
import AppLayout from "@/components/AppLayout";
import { useMemo } from "react";
import { rootStore } from "@/models/rootStore.model";
import { useAppTranslation } from "@/services/i18n";
import { withSessionSsr } from "@/lib/withSessionSsr";

export default observer(() => {
  const { t } = useAppTranslation("profile");
  return (
    <AppLayout>
      <Paper>
        <Stack gap={1}>
          <Typography variant="h6">{t("profile_page_title")}</Typography>
        </Stack>
      </Paper>
    </AppLayout>
  );
});

export const getServerSideProps = withSessionSsr(
  ["vacancies", "profile"],
  ({ props }) => {
    return { props };
  }
);
