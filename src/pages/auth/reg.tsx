import AppLayout from "@/components/AppLayout";
import LoginForm from "@/components/auth/login/loginForm";
import RegistrationForm from "@/components/auth/reg/registrationForm";
import { withSessionSsr } from "@/lib/withSessionSsr";
import theme from "@/theme/theme";
import { Box, Stack } from "@mui/material";
import Image from "next/image";

export default function Login({}) {
  return (
    <AppLayout container={false}>
      <Stack
        alignItems={"center"}
        justifyContent={"space-between"}
        height={"100%"}
        direction={"row"}
      >
        <Box flex={1} display="flex" justifyContent="center" px={2}>
          <RegistrationForm />
        </Box>
        <Box
          flex={1}
          height={"inherit"}
          position={"relative"}
          sx={{
            display: "none",
            [theme.breakpoints.up("md")]: { display: "block" },
          }}
        >
          <Image
            src="/images/Tolyan.png"
            fill
            alt="Background"
            style={{ objectFit: "cover" }}
          />
        </Box>
      </Stack>
    </AppLayout>
  );
}

export const getServerSideProps = withSessionSsr(["form"], ({ props }) => {
  return { props };
});
